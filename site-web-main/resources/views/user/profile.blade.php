@extends('layouts.header-other-page')
@section('title')
<title>{{ Auth::user()->pseudo }}</title>
@endsection

@section('metadata')
<!-- Meta Image et description pour les Réseaux Sociaux -->
<meta property="og:image" content="{{asset('venus_images/logo-nv2_1-removebg-preview.png')}}">
<meta property="og:description" content="Our concern is you">
<!-- Meta URL Canonique (Optionnel) -->
<link rel="canonical" href="https://venusforyoung.com/">
@endsection
@section('produitcategories')
@if(isset($produitCategories))
@foreach($produitCategories as $produitCategory)
<li class="categorie">
    <a href="#">{{$produitCategory->titre}}</a>
</li>
@endforeach
@endif
@endsection
@section('content')

@endsection