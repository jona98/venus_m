@extends('layouts.header-other-page')
@section('title')
<title>Paiement</title>
@endsection

@section('metadata')
<!-- Meta Image et description pour les Réseaux Sociaux -->
<meta property="og:image" content="{{asset('venus_images/logo-nv2_1-removebg-preview.png')}}">
<meta property="og:description" content="Our concern is you">
<!-- Meta URL Canonique (Optionnel) -->
<link rel="canonical" href="https://venusforyoung.com/">
@endsection
@section('produitcategories')
@if(isset($produitCategories))
@foreach($produitCategories as $produitCategory)
<li class="categorie">
    <a href="{{route('produit.byCategory',$produitCategory->id)}}">{{$produitCategory->titre}}</a>
</li>
@endforeach
@endif
@endsection
@section('content')
<section class="uk-section uk-margin-top pt-0">
    <div class="uk-container">
        <div class="uk-grid-small uk-flex uk-flex-middle uk-grid">
            <div class="uk-width-expand@m uk-first-column">
                <h2>Etat de votre paiement</h2>
            </div>
            <div class="uk-width-auto@m mt-20-media-950">
                <!-- <a href="#" class="btn btn-warning" style="background-color: #63016e; color: #fff">Tout voir</a> -->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="alert alert-warning" role="alert">
                    @if(!empty($message))
                    <h3>{{$message}}</h3>
                    @endif
                </div>
            </div>
        </div>

    </div>
</section>
@endsection