<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mon Profil</title>






    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #63016e;
            margin: 0;
            padding: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 100vh;
        }

        .profile-container {
            width: 600px;
            padding: 20px;
            background-color: #ffffff;
            border-radius: 8px;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        }

        .profile-picture {
            width: 100px;
            height: 100px;
            border-radius: 50%;
            background-color: #ddd;
            margin-bottom: 10px;
        }

        .profile-name {
            font-size: 24px;
            font-weight: bold;
            margin-bottom: 10px;
        }

        .profile-email {
            font-size: 16px;
            color: #555;
        }

        .profile-actions {
            margin-top: 20px;
        }

        .profile-actions a {
            display: block;
            margin-bottom: 10px;
            text-decoration: none;
            color: #007bff;
        }
    </style>
</head>

<body>
    <div class="profile-container">
        <div class="profile-picture"></div>
        <div class="profile-name">Votre nom</div>
        <div class="profile-email">Votre email</div>
        <div class="profile-actions">
            <a href="#">Modifier mon profil</a>
            <a href="#">Changer le mot de passe</a>
            <a href="{{route('user.auth.logout')}}">Se déconnecter</a>
        </div>
    </div>
</body>

</html>