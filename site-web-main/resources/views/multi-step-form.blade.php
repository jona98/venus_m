<!DOCTYPE html>
<html>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link
    href="https://fonts.googleapis.com/css?family=Raleway"
    rel="stylesheet"
  />
  <link rel="stylesheet" href="./styles/multi-step-form.css" />
  <link rel="stylesheet" href="./styles/style.css" />
  <link rel="stylesheet" href="./styles/globals.css" />

  <body>
    <form id="regForm" action="/action_page.php">
      <h1>Veuillez nous dire plus sur votre offre</h1>
      <div class="tab">
        <p>
          <label class="uk-form-label" for="title"> Titre </label>
          <input
            name="title"
            id="title"
            type="text"
            class="uk-input uk-form-large"
            value="{title}"
            required
            autocomplete="off"
            autofocus
            onChange="{handleChange}"
          />
        </p>
        <p>
          <label class="uk-form-label" for="price"> Prix </label>
          <input
            name="price"
            id="price"
            type="text"
            class="uk-input uk-form-large"
            value="{price}"
            required
            autocomplete="off"
            onChange="{handleChange}"
          />
        </p>
        <p>
          <label class="uk-form-label" for="category"> Catégorie </label>
          <select
            name="category"
            id="category"
            class="uk-select"
            value="{category}"
            required
            onChange="{handleCategoryChange}"
          >
            <option value="">Sélectionnez une catégorie</option>
          </select>
        </p>
        <p>
          <label class="uk-form-label" for="subcategory">
            Sous-catégorie
          </label>
          <select
            name="subcategory"
            id="subcategory"
            class="uk-select"
            value="{subcategory}"
            required
            onChange="{handleChange}"
          >
            <option value="">Sélectionnez une sous-catégorie</option>
          </select>
        </p>
        <p>
          <label class="uk-form-label" for="category"> Région </label>
          <select
            name="region"
            id="region"
            class="uk-select"
            value="{region}"
            required
            onChange="{handleChange}"
          >
            <option value="">Sélectionnez votre région</option>
          </select>
        </p>
        <p>
          <label class="uk-form-label" for="city"> Ville </label>
          <select
            name="city"
            id="city"
            class="uk-select"
            value="{city}"
            required
            onChange="{handleChange}"
          >
            <option value="">Sélectionnez une sous-catégorie</option>
          </select>
        </p>
        <p>
          <label class="uk-form-label" for="title"> Addresse </label>
          <input
            name="address"
            id="address"
            type="text"
            class="uk-input uk-form-large"
            value="{address}"
            required
            autocomplete="off"
            autofocus
            onChange="{handleChange}"
          />
        </p>
        <p>
          <label class="uk-form-label" for="title"> Description </label>
          <input
            name="description"
            id="description"
            type="text"
            class="uk-input uk-form-large"
            value="{description}"
            required
            autocomplete="off"
            autofocus
            onChange="{handleChange}"
          />
        </p>
      </div>
      <div class="tab">
        <p>
          <label class="uk-form-label" for="city"> Type résidence </label>
          <select
            name="city"
            id="city"
            class="uk-select"
            value="{city}"
            required
            onChange="{handleChange}"
          >
            <option value="">Choisir</option>
          </select>
        </p>
        <p>
          <label class="uk-form-label" for="city"> Type hébergement </label>
          <select
            name="city"
            id="city"
            class="uk-select"
            value="{city}"
            required
            onChange="{handleChange}"
          >
            <option value="">Choisir</option>
          </select>
        </p>
        <p>
          <label class="uk-form-label" for="title"> Classement officiel </label>
          <input
            name="description"
            id="description"
            type="text"
            class="uk-input uk-form-large"
            value="{description}"
            required
            autocomplete="off"
            autofocus
            onChange="{handleChange}"
          />
        </p>
        <p>
          <label class="uk-form-label" for="title"> capacité </label>
          <input
            name="description"
            id="description"
            type="text"
            class="uk-input uk-form-large"
            value="{description}"
            required
            autocomplete="off"
            autofocus
            onChange="{handleChange}"
          />
        </p>
        <p>
          <label class="uk-form-label" for="birthday">Date d'arrivée </label>
          <input
            name="birthday"
            id="birthday"
            type="date"
            class="uk-input uk-form-large"
            value="{description}"
            required
            autocomplete="off"
            autofocus
            onChange="{handleChange}"
          />
        </p>
        <p>
          <label class="uk-form-label" for="birthday">Date de départ: </label>
          <input
            name="birthday"
            id="birthday"
            type="date"
            class="uk-input uk-form-large"
            value="{description}"
            required
            autocomplete="off"
            autofocus
            onChange="{handleChange}"
          />
        </p>
        <p>
          <label class="uk-form-label" for="city"> Avoir une piscine </label>
          <select
            name="city"
            id="city"
            class="uk-select"
            value="{city}"
            required
            onChange="{handleChange}"
          >
            <option value="">Non</option>
            <option value="">Oui</option>
          </select>
        </p>
        <p>
          <label class="uk-form-label" for="city">
            Continent des animaux
          </label>
          <select
            name="city"
            id="city"
            class="uk-select"
            value="{city}"
            required
            onChange="{handleChange}"
          >
            <option value="">Non</option>
            <option value="">Oui</option>
          </select>
        </p>
        <h2 class="mt-20">Ajoutez des photos</h2>
        <div class="choose-file-container">
          <div class="choose-file-content">
            <input type="file" id="actual-btn" hidden />
            <label class="upload-file-label" for="actual-btn">Choisir</label>
            <span id="file-chosen">Choisissez une image</span>
          </div>
          <div class="choose-file-content">
            <input type="file" id="second-btn" hidden />
            <label class="upload-file-label" for="second-btn">Choisir</label>
            <span id="second-file-chosen">Choisissez une image</span>
          </div>
          <div class="choose-file-content">
            <input type="file" id="third-btn" hidden />
            <label class="upload-file-label" for="third-btn">Choisir</label>
            <span id="third-file-chosen">Choisissez une image</span>
          </div>
        </div>
      </div>
      <div style="overflow: auto">
        <div style="float: right">
          <button type="button" id="prevBtn" onclick="nextPrev(-1)">
            Previous
          </button>
          <button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
        </div>
      </div>
      <!-- Circles which indicates the steps of the form: -->
      <div style="text-align: center; margin-top: 40px">
        <span class="step"></span>
        <span class="step"></span>
      </div>
    </form>

    <script src="./js/multi-step-form.js"></script>
  </body>
</html>
