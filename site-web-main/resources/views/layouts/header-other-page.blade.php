<!DOCTYPE html>
<html lang="fr">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    @yield('title')

    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="">
    @yield('metadata')

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" />

    <!-- Stylesheet -->
    <link rel="stylesheet" href="{{ asset ('venus_css/style.css')}}" />
    <link rel="stylesheet" href="{{ asset ('venus_css/header.css')}}" />
    <link rel="stylesheet" href="{{ asset ('venus_css/globals.css')}}" />
    <link rel="stylesheet" href="{{ asset ('venus_css/splide.min.css')}}" />
    <style>
        .cart-icon {
            position: fixed;
            bottom: 20px;
            right: 20px;
            background-color: #63016e;
            color: #fff;
            width: 40px;
            height: 40px;
            border-radius: 50%;
            text-align: center;
            line-height: 40px;
            cursor: pointer;
        }

        .cart-icon i {
            font-size: 20px;
        }

        .cart-count {
            position: absolute;
            top: 0;
            right: 0;
            background-color: #e74c3c;
            color: #fff;
            width: 20px;
            height: 20px;
            border-radius: 50%;
            font-size: 12px;
            line-height: 20px;
        }
    </style>
</head>

<body style="background-color: #f9f8fb">

    <header>
        <nav>
            <a href="/" id="logo">
                <img src="{{ asset ('venus_images/logo-nv2_1-removebg-preview.png')}}" alt="logo" />
            </a>
            <i class="fas fa-bars" id="ham-menu" onclick="toggleMenu()"></i>
            <ul id="nav-bar">
                <li>
                    <a href="/" class="nav-link active">Accueil</a>
                </li>
                <li>
                    <a href="{{route('produit.index')}}" class="nav-link active">Nos produits</a>
                </li>
                <li>
                    <i class="fa-solid fa-cart-shopping"></i>
                    <a href="{{route('cart.checkout')}}" class="nav-link">Panier ({{ session('cart') ? count(session('cart')) : 0 }})</a>
                </li>
                @if (Auth::check())
                <li>
                    <i class="fa-regular fa-heart"></i>
                    <a href="{{url('/favorite')}}" class="nav-link">Mes Favories</a>
                </li>
                <li id="profile-dropdown" class="dropdown">
                    <div class="dropbtn" onclick="dropdownFunction()">
                        <i class="fas fa-user dropbtn"></i>
                        <span class="dropbtn" style="font-weight: bold">{{ Auth::user()->pseudo }}</span>
                        <i class="fas fa-caret-down dropbtn"></i>
                    </div>
                    <div class="dropdown-content" id="myDropdown">
                        <a href="{{route('user.profile')}}" class="personal-link d-block mt-4px mb-4px">Mon profil</a>
                        <a href="/profile/identity" class="personal-link d-block mt-4px mb-4px">Mes commandes</a>
                        <a style="background-color: #63016e; color: #fff" href="{{ route('user.auth.logout') }}">
                            Déconnexion
                        </a>
                    </div>
                </li>
                @else
                <li>
                    <a href="{{route('user.login')}}" class="nav-link login-button uk-button">Se Connecter</a>
                </li>
                @endif
                <!-- <li>
            <button
              id="openModal"
              class="c-white uk-button-primary uk-button m0-media-890 d-flex al-item-center gap-5 font-weight400"
            >
              <svg
                stroke="currentColor"
                fill="none"
                stroke-width="0"
                viewBox="0 0 24 24"
                height="22"
                width="22"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M12 6C12.5523 6 13 6.44772 13 7V11H17C17.5523 11 18 11.4477 18 12C18 12.5523 17.5523 13 17 13H13V17C13 17.5523 12.5523 18 12 18C11.4477 18 11 17.5523 11 17V13H7C6.44772 13 6 12.5523 6 12C6 11.4477 6.44772 11 7 11H11V7C11 6.44772 11.4477 6 12 6Z"
                  fill="currentColor"
                ></path>
                <path
                  fill-rule="evenodd"
                  clip-rule="evenodd"
                  d="M5 22C3.34315 22 2 20.6569 2 19V5C2 3.34315 3.34315 2 5 2H19C20.6569 2 22 3.34315 22 5V19C22 20.6569 20.6569 22 19 22H5ZM4 19C4 19.5523 4.44772 20 5 20H19C19.5523 20 20 19.5523 20 19V5C20 4.44772 19.5523 4 19 4H5C4.44772 4 4 4.44772 4 5V19Z"
                  fill="currentColor"
                ></path>
              </svg>
              Déposer une annonce
            </button>
          </li> -->
            </ul>
        </nav>
        <div class="next-header-container">
            <ul class="menu next-header">
                @yield('produitcategories')
            </ul>
        </div>
        <div id="modal" class="modal" style="display: none">
            <div class="modal-contents">
                <div class="modal-image-wrapper">
                    <span id="closeModal" class="close">
                        <i class="fa-regular fa-circle-xmark"></i>
                    </span>
                </div>
                <div class="modal-content">
                    <div class="mt-20 mb-25 txt-align-center">
                        <h3 class="m-0 mb-25">Que voulez vous publier?</h3>
                        <a href="/multi-step-form.html" id="offerLink" class="offer-request-link offer-link mb-20">Une offre</a>
                        <a href="#" id="requestLink" class="offer-request-link request-link">Une demande</a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    @yield('content')
    <div class="cart-icon" onclick="window.location.href='{{ route('cart.checkout') }}'">
        <i class="fa fa-shopping-cart"></i>
        <span class="cart-count">{{ session('cart') ? count(session('cart')) : 0 }}</span>
    </div>
    <section>
        <footer class="uk-section uk-section-large">
            <div class="uk-container uk-text-muted">
                <div class="uk-child-width-1-2@s uk-child-width-1-5@m uk-grid" data-uk-grid style="justify-content: space-around">
                    <div class="mt-20-media-639">
                        <div class="uk-margin">
                            <h2>
                                <a href="/" class="uk-logo" style="color: #fff;"> Venus </a>
                            </h2>
                        </div>
                        <div class="uk-margin uk-text-small">
                            <p class="ft-size16" style="color: #fff;">
                                Venus.
                            </p>
                        </div>
                        <div class="uk-margin">
                            <div data-uk-grid class="uk-child-width-auto uk-grid-small d-flex">
                                <div class="uk-first-column">
                                    <a href="https://www.facebook.com/" class="uk-icon-link uk-icon" target="_blank" rel="noreferrer" title="Facebook">
                                        <i class="fab fa-facebook" style="font-size: 26px; color: #fff"></i>
                                    </a>
                                </div>
                                <div>
                                    <a href="https://www.instagram.com/" class="uk-icon-link uk-icon" target="_blank" rel="noreferrer" title="Instagram">
                                        <i class="fab fa-instagram" style="font-size: 26px; color: #fff"></i>
                                    </a>
                                </div>
                                <div>
                                    <a href="#" class="uk-icon-link uk-icon" target="_blank" rel="noreferrer" title="Email">
                                        <i class="far fa-envelope" style="font-size: 26px; color: #fff"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <h2 style="color: #fff;">Liens Utiles</h2>
                        <ul class="uk-list uk-text-small">
                            <li>
                                <a class="uk-link-muted" href="/" style="color: #fff;">Accueil</a>
                            </li>
                            <li>
                                <a class="uk-link-muted" href="/about" style="color: #fff;">Collaboration</a>
                            </li>
                            <li>
                                <a class="uk-link-muted" href="/about" style="color: #fff;">Nos conditions d'utilisation (CGU)</a>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <h2 style="color: #fff;">Obtenir L'application</h2>
                        <ul class="uk-list uk-text-small">
                            <li>
                                <a class="uk-link-muted" href="#">
                                    <img class="nav-img" style="width: 100%; height: auto; color: #fff" src="./assets/Google_Play_Store.png" alt="logo" priority loading="eager" />
                                    Télécharger sur Play Store
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <div class="footer-hilexpertiz">
            <p class="m-auto txt-align-center">
                Venus ©2024 All rights reserved | Développé par
                <a href="https://hilexpertiz.africa" target="_blank" rel="noreferrer">Juste Innover</a>
            </p>
        </div>
    </section>

    <!-- Script -->
    <script src="{{ asset ('venus_js/script.js')}}"></script>

    <script src="{{ asset ('venus_js/splide.min.js')}}"></script>
    <script src="{{ asset ('venus_js/splide.js')}}"></script>
</body>

</html>