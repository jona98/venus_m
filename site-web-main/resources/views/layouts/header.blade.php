<header>
  <nav>
    <a href="/" id="logo">
      <img src="{{ asset ('venus_images/logo-nv2_1-removebg-preview.png')}}" alt="logo" />
    </a>
    <i class="fas fa-bars" id="ham-menu" onclick="toggleMenu()"></i>
    <ul id="nav-bar">
      <li>
        <a href="/" class="nav-link active">Accueil</a>
      </li>
      <li>
        <i class="fa-regular fa-heart"></i>
        <a href="{{ url('/favorite')}}" class="nav-link">Mes Favories</a>
      </li>
      <li>
        <a href="/login.html" class="nav-link login-button uk-button">Se Connecter</a>
      </li>

      <li id="profile-dropdown" class="dropdown">
        <div class="dropbtn" onclick="dropdownFunction()">
          <i class="fas fa-user dropbtn"></i>
          <span class="dropbtn" style="font-weight: bold">Nom de l'utilisateur</span>
          <i class="fas fa-caret-down dropbtn"></i>
        </div>
        <div class="dropdown-content" id="myDropdown">
          <a href="/profile/identity" class="personal-link d-block mt-4px mb-4px">Paramètre</a>
          <button class="c-white uk-button-primary uk-button m0-media-890 d-flex al-item-center gap-5 font-weight400" onclick="handleLogout()">
            Déconnexion
          </button>
        </div>
      </li>
    </ul>
  </nav>
  <div class="next-header-container">
    <ul class="menu next-header">
      <li class="categorie">Test</li>
      <li class="categorie">Test</li>
      <li class="categorie">Test</li>
      <li class="categorie">Test</li>
      <li class="categorie">Test</li>
    </ul>
  </div>
  <div id="modal" class="modal" style="display: none">
    <div class="modal-contents">
      <div class="modal-image-wrapper">
        <span id="closeModal" class="close">
          <i class="fa-regular fa-circle-xmark"></i>
        </span>
      </div>
      <div class="modal-content">
        <div class="mt-20 mb-25 txt-align-center">
          <h3 class="m-0 mb-25">Que voulez vous publier?</h3>
          <a href="/multi-step-form.html" id="offerLink" class="offer-request-link offer-link mb-20">Une offre</a>
          <a href="#" id="requestLink" class="offer-request-link request-link">Une demande</a>
        </div>
      </div>
    </div>
  </div>
</header>