<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Checkout</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        .checkout-container {
            margin: 50px auto;
            max-width: 800px;
        }

        .cart-item {
            display: flex;
            justify-content: space-between;
            margin-bottom: 15px;
            border-bottom: 2px solid #63016e; /* Changement d'épaisseur de la bordure */
            padding-bottom: 10px;
        }

        .cart-item img {
            max-width: 100px;
        }

        .form-section {
            margin-top: 30px;
        }

        .form-section h3 {
            margin-bottom: 15px;
        }

        .form-control {
            border-radius: 8px;
            border: 1px solid #63016e;
            padding: 10px;
            margin-top: 10px;
            cursor: pointer;
        }

        .uk-select {
            border-radius: 8px;
            border: 1px solid #63016e;
            padding: 10px;
            margin-top: 10px;
            cursor: pointer;
        }

        .btn-primary, .btn-secondary {
            background-color: #63016e;
            border: 1px solid #63016e;
            border-radius: 8px;
            padding: 10px;
            margin-top: 10px;
            cursor: pointer;
        }

        .btn-secondary:hover {
            background-color: #fff;
            color: #63016e;
        }
    </style>
</head>

<body>
    <div class="checkout-container">
        <h1 class="text-center">Checkout</h1>

        <div class="cart-items">
            <h3>Votre panier</h3>
            @php
            $totalProducts = 0;
            $qty = 0;
            $prix = 0;
            $totalprix = 0;
            $cart = session()->get('cart');

            if ($cart) {
            foreach ($cart as $item) {
            $qty += $item['quantity'];
            $prix += $item['prix'];
            }
            $totalprix = $qty * $prix;
            $totalProducts = count($cart);
            }
            @endphp
            @foreach(session('cart') as $product)
            <div class="cart-item">
                <div class="item-details">
                    <h5>{{ $product['produit_name'] }}</h5>
                    <p>Quantité : {{ $product['quantity'] }}</p>
                </div>
                <div class="item-prix">
                    <p>{{ $product['prix'] }} XOF</p>
                </div>
            </div>
            @endforeach
            <div class="total-prix">
                <h4>Total: {{$totalprix}} XOF</h4>
            </div>
        </div>

        <form action="{{route('commande.create')}}" method="POST">
            @csrf
            @if (session()->has('error'))
            <div class="alert alert-danger alert-dismissible btn-group btn-group-sm d-flex justify-content-center fade show" role="alert">
                <p>{!! session()->get('error') !!}</p>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            <div class="form-section">
                <h3>Infos de contact</h3>
                <div class="form-group">
                    <label for="livraison_adresse">Adresse de livraison</label>
                    <input type="text" class="form-control" id="livraison_adresse" name="livraison_adresse" required>
                </div>
                <div class="form-group">
                    <label for="livraison_mode_livraison_id">Mode de livraison</label>
                    <select class="uk-select font-weight400 c-black" name="livraison_mode_livraison_id" id="livraison_mode_livraison_id">
                        @if(isset($ModeLivraisons))
                        @foreach($ModeLivraisons as $ModeLivraison)
                        <option value="{{$ModeLivraison->id}}" class="font-weight400">{{$ModeLivraison->titre}}</option>
                        @endforeach
                        @endif
                    </select>
                </div>
            </div>

            <div class="form-section">
                <h3>Infos de paiement</h3>
                <div class="form-group">
                    <label for="transaction_mode_paiement_id">Mode de paiement</label>
                    <select class="uk-select font-weight400 c-black" name="transaction_mode_paiement_id" id="transaction_mode_paiement_id">
                        @if(isset($ModePaiements))
                        @foreach($ModePaiements as $ModePaiement)
                        <option value="{{$ModePaiement->id}}" class="font-weight400">{{$ModePaiement->titre}}</option>
                        @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <input type="hidden" name="prix_total" value="{{$totalprix}}">
            <button type="submit" class="btn btn-primary btn-block">Commander</button>
            <a href="{{route('landingPage')}}" class="btn btn-secondary btn-block" style="margin-top: 10px;">Retour</a>
        </form>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>

</html>
