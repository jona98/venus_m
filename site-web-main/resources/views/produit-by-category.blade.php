@extends('layouts.header-other-page')
@section('title')
<title>Boutique</title>
@endsection

@section('metadata')
<!-- Meta Image et description pour les Réseaux Sociaux -->
<meta property="og:image" content="{{asset('venus_images/logo-nv2_1-removebg-preview.png')}}">
<meta property="og:description" content="Our concern is you">
<!-- Meta URL Canonique (Optionnel) -->
<link rel="canonical" href="https://venusforyoung.com/">
@endsection
@section('produitcategories')
@if(isset($produitCategories))
@foreach($produitCategories as $produitCategory)
<li class="categorie">
    <a href="{{route('produit.byCategory',$produitCategory->id)}}">{{$produitCategory->titre}}</a>
</li>
@endforeach
@endif
@endsection
@section('content')
<section class="uk-section uk-margin-top pt-0">
    <div class="uk-container">
        <div class="container mt-5">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <form action="/search" method="GET" class="search-form">
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="pays_annonce">Nom</label>
                                <input type="text" class="form-control" placeholder="Nom du produit">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="type_annonce">Type de produit</label>
                                <select id="type_annonce" name="type_annonce" class="form-control">
                                    <option selected>Choisir un type</option>
                                    @if(isset($produitCategories))
                                    @foreach($produitCategories as $produitCategory)
                                    <option value="{{$produitCategory->id}}">{{$produitCategory->titre}}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="produit_category">Trier par</label>
                                <select id="produit_category" name="produit_category" class="form-control">
                                    <option selected>Faites un choix</option>
                                    <option value="1">Le plus récent</option>
                                    <option value="1">Prix décroissant</option>
                                    <option value="1">Prix croissant</option>
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block" style="background-color: #63016e; color: #fff">Rechercher</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="uk-grid-small uk-flex uk-flex-middle uk-grid">
            <div class="uk-width-expand@m uk-first-column">
                <h2>Nos produits</h2>
            </div>
            <div class="uk-width-auto@m mt-20-media-950">
                <!-- <a href="#" class="btn btn-warning" style="background-color: #63016e; color: #fff">Tout voir</a> -->
            </div>
        </div>

        <div class="uk-child-width-1-2@s uk-child-width-1-3@m uk-grid-match uk-margin-medium-top uk-grid">
            <div style="width: 100%; overflow: hidden">
                <div class="uk-child-width-1-2@s uk-child-width-1-3@m uk-child-width-1-4@l uk-grid-match uk-grid mt-0">
                    <!-- card -->
                    @if(isset($produits))
                    @foreach($produits as $produit)
                    <div class="mt-30">
                        <div class="uk-card uk-card-small uk-card-default uk-card-hover uk-border-rounded-large uk-overflow-hidden">
                            <div class="uk-card-media-top uk-inline uk-light">
                                <img src="{{ 'https://api.venusforyoung.com' . $produit->cover }}" alt="image de l'évenement" class="max-height-154 media-500-max-width-173 object-fit-cover" />
                                <div class="uk-position-cover uk-overlay-xlight"></div>
                                <div class="uk-position-small uk-position-top-left">
                                    <span class="uk-label uk-text-bold uk-text-price">
                                        {{$produit->prix}} XOF
                                    </span>
                                </div>
                                <div class="uk-position-small uk-position-top-right card-icon-bgc">
                                    <i class="fa-regular fa-heart cursor-pointer uk-icon-button uk-like uk-position-z-index uk-position-relative card-icon-bg-none"></i>
                                </div>
                            </div>
                            <div class="uk-card-body">
                                <h3 class="uk-card-title uk-margin-small-bottom"><a href="{{route('produit.detail',$produit->id)}}" style="color: #333;">{{ Str::limit($produit->titre, 100) }}</a></h3>
                                <div class="uk-text-muted uk-text-small">
                                    {{ Str::limit($produit->description, 150, '...') }}
                                </div>
                                <div class="uk-text-muted uk-text-xxsmall uk-rating uk-margin-small-top">
                                    <i class="fa-regular fa-star"></i>
                                    <i class="fa-regular fa-star"></i>
                                    <i class="fa-regular fa-star"></i>
                                    <i class="fa-regular fa-star"></i>
                                    <i class="fa-regular fa-star"></i>
                                </div>
                            </div>
                            <a href="{{route('produit.detail',$produit->id)}}" class="uk-position-cover c-transparent">
                                Voir plus
                            </a>
                        </div>
                    </div>
                    @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@endsection