<!DOCTYPE html>
<html lang="fr">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Accueil</title>
    <!-- Font Awesome Icons -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" />

    <!-- Stylesheet -->
    <link rel="stylesheet" href="{{ asset ('venus_css/style.css')}}" />
    <link rel="stylesheet" href="{{ asset ('venus_css/globals.css')}}" />

</head>


<body style="background-color: #f8fafb">

    <div class="uk-grid-collapse uk-grid" style="height: 100vh" data-uk-grid>
        <div class="uk-width-1-2@m uk-padding-large uk-flex uk-flex-middle uk-flex-center" data-uk-height-viewport>
            <div class="uk-width-3-4@s">
                <div class="uk-text-center uk-margin-medium-bottom">
                    <h1 class="uk-letter-spacing-small">Inscription</h1>
                </div>

                <form action="{{route('register')}}" method="post">
                    @csrf
                    <div class="uk-width-1-1 uk-margin">
                        <label class="uk-form-label" for="name">Pseudo</label>
                        <input name="pseudo" id="pseudo" type="text" class="uk-input uk-form-large" required autocomplete="pseudo" autofocus />
                    </div>
                    <div class="uk-width-1-1 uk-margin">
                        <label class="uk-form-label" for="telephone">Numéro de téléphone</label>
                        <input name="telephone" id="telephone" class="uk-input uk-form-large" type="number" placeholder="Numéro" required />
                    </div>
                    <div class="uk-width-1-1 uk-margin">
                        <label class="uk-form-label" for="genre_id">Genre</label>
                        <select class="form-control" name="genre_id" id="genre_id">
                            @if(isset($genres))
                            @foreach($genres as $genre)
                            <option value="{{$genre->id}}">{{$genre->titre}}</option>
                            @endforeach
                        </select>
                        @endif
                    </div>
                    <div class="uk-width-1-1 uk-margin">
                        <label class="uk-form-label" for="naissance">Date de naissance</label>
                        <input name="naissance" id="naissance" class="uk-input uk-form-large" type="text" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" placeholder="aaaa-mm-jj" required />
                    </div>
                    <div class="uk-width-1-1 uk-margin">
                        <label class="uk-form-label" for="statut_professionnel">Statut professionnel</label>
                        <input name="statut_professionnel" id="statut_professionnel" class="uk-input uk-form-large" type="text" placeholder="Votre profession" required />
                    </div>
                    <div class="uk-width-1-1 uk-margin">
                        <label class="uk-form-label" for="quartier">Quartier</label>
                        <input name="quartier" id="quartier" class="uk-input uk-form-large" type="text" placeholder="Quartier de résidence" required />
                    </div>
                    <input name="pays_id" type="hidden" value="1" required />
                    <input name="role_id" type="hidden" value="1" required />
                    <div class="uk-width-1-1 uk-margin">
                        <label class="uk-form-label" for="password">Mot de Passe</label>
                        <input name="password" id="password" class="uk-input uk-form-large" type="password" placeholder="Min 8 charactères" required />
                    </div>
                    <div class="uk-width-1-1 uk-margin">
                        <label class="uk-form-label" for="password_confirmation">Confirmez le Mot de Passe</label>
                        <input name="password_confirmation" id="password_confirmation" class="uk-input uk-form-large" type="password" placeholder="Min 8 charactères" required />
                    </div>
                    @if (Laravel\Jetstream\Jetstream::hasTermsAndPrivacyPolicyFeature())
                    <div class="mt-4">
                        <x-label for="terms">
                            <div class="flex items-center">
                                <x-checkbox name="terms" id="terms" required />

                                <div class="ms-2">
                                    {!! __('I agree to the :terms_of_service and :privacy_policy', [
                                    'terms_of_service' => '<a target="_blank" href="'.route('terms.show').'" class="underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">'.__('Terms of Service').'</a>',
                                    'privacy_policy' => '<a target="_blank" href="'.route('policy.show').'" class="underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">'.__('Privacy Policy').'</a>',
                                    ]) !!}
                                </div>
                            </div>
                        </x-label>
                    </div>
                    @endif
                    <div class="uk-width-1-1 uk-text-center">
                        <button type="submit" class="uk-button uk-button-primary uk-button-large" style="background-color: #63016e; color: #fff">
                            S'inscrire
                        </button>
                    </div>
                    <div class="uk-width-1-1 uk-margin uk-text-center">
                        <p class="uk-text-small uk-margin-remove">
                            En vous inscrivant vous acceptez nos
                            <a href="#" class="uk-link-border">termes et conditions d'utilisation.</a>
                        </p>
                    </div>
                </form>
            </div>
        </div>
        <div class="uk-width-1-2@m uk-padding-large uk-flex uk-flex-middle uk-flex-center uk-light uk-background-cover uk-background-norepeat uk-background-blend-overlay uk-overlay-blend" style="
          background-image: url(https://venusforyoung.com/venus_images/img.jpg);
        " data-uk-height-viewport>
            <div>
                <div class="uk-text-center">
                    <h2 class="uk-h1 uk-letter-spacing-small">
                        Besoin d'aide ?
                    </h2>
                </div>
                <div class="uk-width-1-1 uk-text-center">
                    <a href="#" class="uk-button uk-button-primary uk-button-large">Ecrivez-nous sur Whatsapp</a>
                </div>
            </div>
        </div>
    </div>
</body>

</html>