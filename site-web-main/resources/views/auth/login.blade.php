<!DOCTYPE html>
<html lang="fr">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Accueil</title>
    <!-- Font Awesome Icons -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" />
    <!-- Stylesheet -->
    <link rel="stylesheet" href="{{ asset ('venus_css/style.css')}}" />
    <link rel="stylesheet" href="{{ asset ('venus_css/globals.css')}}" />
</head>

<body style="background-color: #f8fafb">
    <div style="height: 100vh">
        <div class="uk-grid-collapse uk-grid login-wrapper login-container" data-uk-grid style="height: 100%">
            <div class="uk-width-1-2@m uk-padding-large uk-flex uk-flex-middle uk-flex-center" data-uk-height-viewport>
                <div class="uk-width-3-4@s">
                    <div class="uk-text-center uk-margin-medium-bottom mt-20">
                        <h1 class="uk-letter-spacing-small">Se Connecter</h1>
                    </div>
                    <form action="{{route('user.auth.login')}}" method="post">
                        @csrf
                        <div class="uk-width-1-1 uk-margin">
                            <label class="uk-form-label" for="email">Numero</label>
                            <input name="telephone" id="telephone" class="uk-input uk-form-large" type="number" placeholder="Numéro" autocomplete="telephone" required />
                        </div>
                        <div class="uk-width-1-1 uk-margin">
                            <label class="uk-form-label" for="password">Mot de Passe</label>
                            <input class="uk-input uk-form-large" id="password" type="password" name="password" autocomplete="current-password" required placeholder="Min 8 charactères" />
                        </div>
                        <div class="uk-width-1-1 uk-margin uk-text-center">
                            <a class="uk-text-small uk-link-muted" href="#">Mot de Passe Oublier?</a>
                        </div>
                        <div class="uk-width-1-1 uk-text-center">
                            <button type="submit" class="uk-button uk-button-primary uk-button-large" style="background-color: #63016e; color: #fff;">
                                Se Connecter
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="uk-width-1-2@m uk-padding-large uk-flex uk-flex-middle uk-flex-center uk-light uk-background-cover uk-background-norepeat uk-background-blend-overlay uk-overlay-blend" style="
            background-image: url(https://venusforyoung.com/venus_images/img.jpg);
          " data-uk-height-viewport>
                <div>
                    <div class="uk-text-center">
                        <h2 class="uk-h1 uk-letter-spacing-small">Nous rejoindre</h2>
                    </div>
                    <div class="uk-margin-top uk-margin-medium-bottom uk-text-center">
                        <p>Inscrivez vous maintenant pour rejoindre notre communauté</p>
                    </div>
                    <div class="uk-width-1-1 uk-text-center">
                        <a href="{{route('user.register')}}" class="uk-button uk-button-primary uk-button-large" style="background-color: #63016e; color: #fff;">S'inscrire</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>