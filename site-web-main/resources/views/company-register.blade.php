<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Accueil</title>
    <!-- Font Awesome Icons -->

    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css"
    />
    <!-- Stylesheet -->
    <link rel="stylesheet" href="{{ asset ('venus_css/style.css')}}" />
    <link rel="stylesheet" href="{{ asset ('venus_css/globals.css')}}" />
  </head>

  <body style="background-color: #f8fafb">
    <div class="uk-grid-collapse uk-grid" data-uk-grid>
      <div
        class="uk-width-1-2@m uk-padding-large uk-flex uk-flex-middle uk-flex-center"
        data-uk-height-viewport
      >
        <div class="uk-width-3-4@s">
          <div class="uk-text-center uk-margin-medium-bottom">
            <h1 class="uk-letter-spacing-small">Compte entreprise</h1>
          </div>
          <form
            method="POST"
            action="{{ route('register') }}"
            onSubmit="{handleSubmit}"
          >
            <!-- CSRF token -->
            <input type="hidden" name="_token" />

            <div class="uk-width-1-1 uk-margin">
              <label class="uk-form-label" for="numero_cfe">Numéro CFE</label>
              <input
                name="numero_cfe"
                id="numero_cfe"
                type="text"
                class="uk-input uk-form-large"
                value=""
                required
              />
            </div>
            <div class="uk-width-1-1 uk-margin">
              <label class="uk-form-label" for="nom">Nom de la société</label>
              <input
                name="nom"
                id="nom"
                type="text"
                class="uk-input uk-form-large"
                value=""
                required
              />
            </div>
            <div class="uk-width-1-1 uk-margin">
              <label class="uk-form-label" for="categorie"
                >Catégorie associée à votre activité</label
              >
              <select
                name="categorie"
                id="categorie"
                class="uk-select uk-form-large"
                value=""
                required
              >
                <option>categorie</option>
              </select>
            </div>
            <div class="uk-width-1-1 uk-margin">
              <label class="uk-form-label" for="entreprise_phone"
                >Numéro de téléphone de l'entreprise</label
              >
              <input
                name="entreprise_phone"
                id="entreprise_phone"
                class="uk-input uk-form-large"
                type="tel"
                placeholder="Numéro de téléphone"
                value=""
                required
              />
            </div>
            <div class="uk-width-1-1 uk-margin">
              <label class="uk-form-label" for="adresse"
                >Adresse de l'entreprise</label
              >
              <input
                name="adresse"
                id="adresse"
                class="uk-input uk-form-large"
                type="text"
                placeholder="exemple: Avepozo"
                value="{adresse}"
                required
              />
            </div>
            <div class="uk-width-1-1 uk-margin">
              <label class="uk-form-label" for="region">Région</label>
              <select
                name="region"
                id="region"
                class="uk-select uk-form-large"
                value=""
                required
              >
                <option key="{region.id}" value="{region.id}">region</option>
              </select>
            </div>
            <div class="uk-width-1-1 uk-margin">
              <label class="uk-form-label" for="name"
                >Nom complet du propriétaire</label
              >
              <input
                name="name"
                id="name"
                type="text"
                class="uk-input uk-form-large"
                value="{name}"
                required
              />
            </div>
            <div class="uk-width-1-1 uk-margin">
              <label class="uk-form-label" for="phone"
                >Numéro personnel du propriétaire</label
              >
              <input
                name="phone"
                id="phone"
                class="uk-input uk-form-large"
                type="tel"
                placeholder="Numéro personnel du propriétaire"
                value=""
                required
              />
            </div>
            <div class="uk-width-1-1 uk-margin">
              <label class="uk-form-label" for="password">Mot de Passe</label>
              <input
                name="password"
                id="password"
                class="uk-input uk-form-large"
                type="password"
                placeholder="Min 8 caractères"
                value=""
              />
            </div>
            <div class="uk-width-1-1 uk-margin">
              <label class="uk-form-label" for="password_confirmation"
                >Confirmez le Mot de Passe</label
              >
              <input
                name="password_confirmation"
                id="password_confirmation"
                class="uk-input uk-form-large"
                type="password"
                placeholder="Min 8 caractères"
                value=""
                required
              />
            </div>
            <div class="uk-width-1-1 uk-text-center">
              <button
                type="submit"
                class="uk-button uk-button-primary uk-button-large"
              >
                S'inscrire
              </button>
            </div>
            <div class="uk-width-1-1 uk-margin uk-text-center">
              <p class="uk-text-small uk-margin-remove">
                En vous inscrivant vous acceptez nos
                <a href="#" class="uk-link-border"
                  >termes et conditions d'utilisation.</a
                >
              </p>
            </div>
          </form>
        </div>
      </div>
      <div
        class="uk-width-1-2@m uk-padding-large uk-flex uk-flex-middle uk-flex-center uk-light uk-background-cover uk-background-norepeat uk-background-blend-overlay uk-overlay-blend"
        style="
          background-image: url(https://source.unsplash.com/nF8xhLMmg0c/680x1000);
        "
        data-uk-height-viewport
      >
        <div>
          <div class="uk-text-center">
            <h2 class="uk-h1 uk-letter-spacing-small">
              S'inscrire en tant qu'entreprise
            </h2>
          </div>
          <div class="uk-width-1-1 uk-text-center">
            <a
              href="/registe"
              class="uk-button uk-button-primary uk-button-large"
              >Compte client</a
            >
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
