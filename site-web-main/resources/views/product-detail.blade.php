@extends('layouts.header-other-page')

@section('title')
<title>{{$produit->titre}}</title>
@endsection

@section('metadata')
<!-- Meta Image et description pour les Réseaux Sociaux -->
<meta property="og:image" content="{{asset('venus_images/logo-nv2_1-removebg-preview.png')}}">
<meta property="og:description" content="Our concern is you">
<!-- Meta URL Canonique (Optionnel) -->
<link rel="canonical" href="{{ url()->current() }}">
@endsection
@section('produitcategories')
@if(isset($produitCategories))
@foreach($produitCategories as $produitCategory)
<li class="categorie">
  <a href="#">{{$produitCategory->titre}}</a>
</li>
@endforeach
@endif
@endsection
@section('content')
<section class="uk-section">
  <div class="uk-container">
    <div class="uk-grid-large uk-grid">
      <div class="uk-width-expand@m">
        <div class="splide" data-splide='{ "perPage": 1, "perMove": 1, "type": "loop", "rewind": true, "keyboard": true, "gap": "1rem", "pagination": false, "padding": "2rem" }'>
          <div class="splide__track">
            <ul class="splide__list">
              @if($produit->images !== null)
              @foreach($produit->images as $image)
              <li class="splide__slide" style="width: 100%;">
                <div style="width: 100%; height: 420px; position: relative;">
                  <img src="{{ 'https://api.venusforyoung.com' . $image->url }}" alt="Image du produit" class="lazyload object-fit-cover" style="border-radius: 20px; height: 100%;">
                </div>
              </li>
              @endforeach
              @endif
            </ul>
          </div>
        </div>
        <div class="uk-article mt-20">
          <h2 class="c-orange m-0" style="font-weight: 600; font-size: 2.7rem; border-radius: 10px; border: 3px solid #63016e; width: 220px; background-color: white;">{{$produit->prix}} XOF</h2>
          <h3 class="m-0 mt-10">Détails du produit</h3>
          <!-- <p class="m-0"> {{$produit->sous_categorie->titre}}</p> -->
          <p class="m-0">{{$produit->description}}</p>
          <p class="m-0">type de produit : {{$produit->sous_categorie->titre}}</p>
        </div>
      </div>
      <div class="uk-width-1-3@m">
        <div data-uk-sticky="offset: 100; bottom: true; media: @m" >
          <div class="uk-card uk-card-default uk-card-body uk-width-1-1 uk-border-rounded-large">
            <div class="mt-2">
              <h3 class="mb-0">Commander maintenant</h3>
            </div>
            <h4 class="mt-20">
              <!-- Insérer ici le composant PhoneButton avec le numéro de téléphone -->
              

            </h4>
            <!-- Insérer ici le composant WhatsAppButton avec le numéro de téléphone -->
            

            <hr style="border: solid #63016e;">
          
            <div class="d-flex gap-10 al-item-center">
              <h4 class="m-0">
                Quantité disponible :
              </h4>
              <h4 class="m-0 c-orange" >@if($produit->quantity_available > 0) {{$produit->quantity_available}} @else En rupture de stock @endif</h4>
            </div>
            <hr style="border: solid #63016e;">
            <div class="d-flex gap-10">
              <form action="{{route('cart.addItem')}}" method="post">
                <div class="d-flex gap-10">
                  <MdDomain size="40"></MdDomain>

                  <div>
                    @if(session()->has('success'))
                    <h5 class="uk-comment-title uk-margin-remove">{{ session()->get('success') }}</h5>
                    @endif
                    @if(session()->has('error'))
                    <h5 class="uk-comment-title uk-margin-remove">{{ session()->get('error') }}</h5>
                    @endif

                    <div class="uk-comment-meta uk-margin-remove">
                      @csrf
                      <input min="1" max="10" value="1" type="number" name="quantity" style="border-radius: 8px; border: 1px solid #63016e; padding: 10px; margin-top: 10px; cursor: pointer;">
                      <input value="{{$produit->titre}}" type="hidden" name="produit_name">
                      <input value="{{$produit->id}}" type="hidden" name="produit_id">
                      <input value="{{$produit->prix}}" type="hidden" name="prix">
                      <!-- <a class="uk-link-reset" href="#">
                      {eventsdata?.region?.libelle}
                    </a> -->
                    </div>
                    
                  </div>
                </div>
                <div class="d-flex gap-10">
                  <MdOutlineLocationOn size="40" style="color: #56667b;"></MdOutlineLocationOn>
                  <div>
                    <br>
                    <button type="submit" style="background-color: #63016e; color: #fff; border : 4px solid #63016e; border-radius: 8px 8px 8px 5px; padding: 5px; cursor: pointer;">Ajouter au panier</button>
                  </div>

                </div>
              </form>
            </div>
            <hr style="border: solid #63016e;">
            <div class="other" style=" display : flex; justify-content : center; align-items : center; flex-direction : row; gap : 2vh;">
            <a href="tel:{{ $produit->phone_number }}" class="uk-button uk-button-success" style="display: inline-flex; align-items: center;">
                <i class="fas fa-phone-alt" style="margin-right: 10px;"></i> Appeler
              </a>
              <a href="https://wa.me/{{ $produit->phone_number }}" target="_blank" class="uk-button uk-button-success" style="display: inline-flex; align-items: center; margin-top: 10px;">
              <i class="fab fa-whatsapp" style="margin-right: 10px;"></i> WhatsApp
            </a>
            </div>
            <!-- <h3 class="mt-0">Publié par:</h3>
            <div class="d-flex gap-10">
              <img src="" alt="User Profile">
              <p><b>{eventsdata?.user?.name}</b></p>
            </div>
            <ul class="uk-comment-list">
              <li>
                <article class="uk-comment uk-visible-toggle" tabindex="-1">
                  <div class="uk-width-expand">
                    <h4 class="uk-comment-title uk-margin-remove">
                      <a class="uk-link-reset" href="#">Membre depuis</a>
                    </h4>
                    <div class="uk-comment-meta uk-margin-remove">
                      <a class="uk-link-reset" href="#">
                        {formattedStartMemberDate}
                      </a>
                    </div>
                    <div class="uk-rating mb-10">
                      <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.8"></span>
                      <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.8"></span>
                      <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.8"></span>
                      <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.8"></span>
                      <span data-uk-icon="icon: star; ratio: 0.8"></span>
                    </div>
                  </div>
                </article>
              </li>
            </ul>
            <ul class="uk-list uk-margin-small-top pt-10">
              <li>
                <span class="uk-margin-small-right" data-uk-icon="clock">
                  <BsCalendarDate />
                </span>
                Publié le: {formattedStartDate}
              </li>
            </ul> -->
          </div>
          <h3>Suivez - nous</h3>
          <div class="uk-margin">
            <div data-uk-grid class="uk-child-width-auto uk-grid-small d-flex">
              <div>
                <a href="#" target="_blank" class="uk-icon-button" style="background-color: rgba(99, 1, 110, 1); color: #fff; display: inline-flex; align-items: center; justify-content: center; width: 40px; height: 40px; border-radius: 50%;">
                    <i class="fab fa-facebook-f"></i>
                  </a>
              </div>
              <div>
              <a href="#" target="_blank" class="uk-icon-button" style="background-color: rgba(99, 1, 110, 1); color: #fff ; display: inline-flex; align-items: center; justify-content: center; width: 40px; height: 40px; border-radius: 50%;">
                    <i class="fab fa-twitter"></i>
                  </a>
              </div>
              <div>
              <a href="#" target="_blank" class="uk-icon-button" style="background-color: rgba(99, 1, 110, 1); color: #fff; display: inline-flex; align-items: center; justify-content: center; width: 40px; height: 40px; border-radius: 50%;">
                    <i class="fab fa-linkedin-in"></i>
                  </a>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
@endsection