<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Accueil</title>
    <!-- Font Awesome Icons -->

    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css"
    />
    <!-- Stylesheet -->
    <link rel="stylesheet" href="{{ asset ('venus_css/style.css')}}" />
    <link rel="stylesheet" href="{{ asset ('venus_css/globals.css')}}" />
    <link rel="stylesheet" href="{{ asset ('venus_css/header.css')}}" />
  </head>
  <body style="background-color: #f8fafb">

  <header>
      <nav>
        <a href="/" id="logo">
          <img src="{{ asset ('venus_images/logo-nv2_1-removebg-preview.png')}}" alt="logo" />
        </a>
        <i class="fas fa-bars" id="ham-menu" onclick="toggleMenu()"></i>
        <ul id="nav-bar">
          <li>
            <a href="/" class="nav-link active">Accueil</a>
          </li>
          <li>
            <i class="fa-regular fa-heart"></i>
            <a href="{{ url('/favorite')}}" class="nav-link">Mes Favories</a>
          </li>
          <li>
            <a href="{{ url('/loginf')}}" class="nav-link login-button uk-button"
              >Se Connecter</a
            >
          </li>
          <li>
            <button
              id="openModal"
              class="c-white uk-button-primary uk-button m0-media-890 d-flex al-item-center gap-5 font-weight400"
            >
              <svg
                stroke="currentColor"
                fill="none"
                stroke-width="0"
                viewBox="0 0 24 24"
                height="22"
                width="22"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M12 6C12.5523 6 13 6.44772 13 7V11H17C17.5523 11 18 11.4477 18 12C18 12.5523 17.5523 13 17 13H13V17C13 17.5523 12.5523 18 12 18C11.4477 18 11 17.5523 11 17V13H7C6.44772 13 6 12.5523 6 12C6 11.4477 6.44772 11 7 11H11V7C11 6.44772 11.4477 6 12 6Z"
                  fill="currentColor"
                ></path>
                <path
                  fill-rule="evenodd"
                  clip-rule="evenodd"
                  d="M5 22C3.34315 22 2 20.6569 2 19V5C2 3.34315 3.34315 2 5 2H19C20.6569 2 22 3.34315 22 5V19C22 20.6569 20.6569 22 19 22H5ZM4 19C4 19.5523 4.44772 20 5 20H19C19.5523 20 20 19.5523 20 19V5C20 4.44772 19.5523 4 19 4H5C4.44772 4 4 4.44772 4 5V19Z"
                  fill="currentColor"
                ></path>
              </svg>
              Déposer une annonce
            </button>
          </li>
          <li id="profile-dropdown" class="dropdown">
            <div class="dropbtn" onclick="dropdownFunction()">
              <i class="fas fa-user dropbtn"></i>
              <span class="dropbtn" style="font-weight: bold"
                >Nom de l'utilisateur</span
              >
              <i class="fas fa-caret-down dropbtn"></i>
            </div>
            <div class="dropdown-content" id="myDropdown">
              <a
                href="/profile/identity"
                class="personal-link d-block mt-4px mb-4px"
                >Paramètre</a
              >
              <button
                class="c-white uk-button-primary uk-button m0-media-890 d-flex al-item-center gap-5 font-weight400"
                onclick="handleLogout()"
              >
                Déconnexion
              </button>
            </div>
          </li>
        </ul>
      </nav>
      <div class="next-header-container">
        <ul class="menu next-header">
          <li class="categorie">Test</li>
          <li class="categorie">Test</li>
          <li class="categorie">Test</li>
          <li class="categorie">Test</li>
          <li class="categorie">Test</li>
        </ul>
      </div>
      <div id="modal" class="modal" style="display: none">
        <div class="modal-contents">
          <div class="modal-image-wrapper">
            <span id="closeModal" class="close">
              <i class="fa-regular fa-circle-xmark"></i>
            </span>
          </div>
          <div class="modal-content">
            <div class="mt-20 mb-25 txt-align-center">
              <h3 class="m-0 mb-25">Que voulez vous publier?</h3>
              <a
                href="/multi-step-form.html"
                id="offerLink"
                class="offer-request-link offer-link mb-20"
                >Une offre</a
              >
              <a
                href="#"
                id="requestLink"
                class="offer-request-link request-link"
                >Une demande</a
              >
            </div>
          </div>
        </div>
      </div>
    </header>


    <div class="uk-section pt-40">
      <div class="uk-container">
        <div class="uk-grid-small uk-flex uk-flex-middle uk-grid">
          <div class="uk-width-expand@m uk-first-column">
            <h2>Vos favories</h2>
          </div>
        </div>
        <div
          class="uk-child-width-1-2@s uk-child-width-1-3@m uk-grid-match uk-margin-medium-top uk-grid"
        >
          <div style="width: 100%; overflow: hidden">
            <div
              class="uk-child-width-1-2@s uk-child-width-1-3@m uk-child-width-1-4@l uk-grid-match uk-grid mt-0"
            >
              <!-- card -->
              <div class="mt-30">
                <div
                  class="uk-card uk-card-small uk-card-default uk-card-hover uk-border-rounded-large uk-overflow-hidden"
                >
                  <div class="uk-card-media-top uk-inline uk-light">
                    <img
                      src="https://www.la-corse-autrement.com/wp-content/uploads/2021/07/DJI_0408-1128x484.jpg"
                      alt="image de l'évenement"
                      class="max-height-154 media-500-max-width-173 object-fit-cover"
                    />
                    <div class="uk-position-cover uk-overlay-xlight"></div>
                    <div class="uk-position-small uk-position-top-left">
                      <span class="uk-label uk-text-bold uk-text-price">
                        10/07/2023
                      </span>
                    </div>
                    <div
                      class="uk-position-small uk-position-top-right card-icon-bgc"
                    >
                      <i
                        class="fa-regular fa-heart cursor-pointer uk-icon-button uk-like uk-position-z-index uk-position-relative card-icon-bg-none"
                      ></i>
                    </div>
                  </div>
                  <div class="uk-card-body">
                    <h3 class="uk-card-title uk-margin-small-bottom">Titre</h3>
                    <div class="uk-text-muted uk-text-small">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Ratione expedita quaerat laboriosam soluta eligendi nemo.
                    </div>
                    <div
                      class="uk-text-muted uk-text-xxsmall uk-rating uk-margin-small-top"
                    >
                      <i class="fa-regular fa-star"></i>
                      <i class="fa-regular fa-star"></i>
                      <i class="fa-regular fa-star"></i>
                      <i class="fa-regular fa-star"></i>
                      <i class="fa-regular fa-star"></i>
                    </div>
                  </div>
                  <a
                    href="./product-detail.html"
                    class="uk-position-cover c-transparent"
                  >
                    details de l'évenement
                  </a>
                </div>
              </div>
              <div class="mt-30">
                <div
                  class="uk-card uk-card-small uk-card-default uk-card-hover uk-border-rounded-large uk-overflow-hidden"
                >
                  <div class="uk-card-media-top uk-inline uk-light">
                    <img
                      src="https://www.la-corse-autrement.com/wp-content/uploads/2021/07/DJI_0408-1128x484.jpg"
                      alt="image de l'évenement"
                      class="max-height-154 media-500-max-width-173 object-fit-cover"
                    />
                    <div class="uk-position-cover uk-overlay-xlight"></div>
                    <div class="uk-position-small uk-position-top-left">
                      <span class="uk-label uk-text-bold uk-text-price">
                        10/07/2023
                      </span>
                    </div>
                    <div
                      class="uk-position-small uk-position-top-right card-icon-bgc"
                    >
                      <i
                        class="fa-regular fa-heart cursor-pointer uk-icon-button uk-like uk-position-z-index uk-position-relative card-icon-bg-none"
                      ></i>
                    </div>
                  </div>
                  <div class="uk-card-body">
                    <h3 class="uk-card-title uk-margin-small-bottom">Titre</h3>
                    <div class="uk-text-muted uk-text-small">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Ratione expedita quaerat laboriosam soluta eligendi nemo.
                    </div>
                    <div
                      class="uk-text-muted uk-text-xxsmall uk-rating uk-margin-small-top"
                    >
                      <i class="fa-regular fa-star"></i>
                      <i class="fa-regular fa-star"></i>
                      <i class="fa-regular fa-star"></i>
                      <i class="fa-regular fa-star"></i>
                      <i class="fa-regular fa-star"></i>
                    </div>
                  </div>
                  <a
                    href="./product-detail.html"
                    class="uk-position-cover c-transparent"
                  >
                    details de l'évenement
                  </a>
                </div>
              </div>
              <div class="mt-30">
                <div
                  class="uk-card uk-card-small uk-card-default uk-card-hover uk-border-rounded-large uk-overflow-hidden"
                >
                  <div class="uk-card-media-top uk-inline uk-light">
                    <img
                      src="https://www.la-corse-autrement.com/wp-content/uploads/2021/07/DJI_0408-1128x484.jpg"
                      alt="image de l'évenement"
                      class="max-height-154 media-500-max-width-173 object-fit-cover"
                    />
                    <div class="uk-position-cover uk-overlay-xlight"></div>
                    <div class="uk-position-small uk-position-top-left">
                      <span class="uk-label uk-text-bold uk-text-price">
                        10/07/2023
                      </span>
                    </div>
                    <div
                      class="uk-position-small uk-position-top-right card-icon-bgc"
                    >
                      <i
                        class="fa-regular fa-heart cursor-pointer uk-icon-button uk-like uk-position-z-index uk-position-relative card-icon-bg-none"
                      ></i>
                    </div>
                  </div>
                  <div class="uk-card-body">
                    <h3 class="uk-card-title uk-margin-small-bottom">Titre</h3>
                    <div class="uk-text-muted uk-text-small">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Ratione expedita quaerat laboriosam soluta eligendi nemo.
                    </div>
                    <div
                      class="uk-text-muted uk-text-xxsmall uk-rating uk-margin-small-top"
                    >
                      <i class="fa-regular fa-star"></i>
                      <i class="fa-regular fa-star"></i>
                      <i class="fa-regular fa-star"></i>
                      <i class="fa-regular fa-star"></i>
                      <i class="fa-regular fa-star"></i>
                    </div>
                  </div>
                  <a
                    href="./product-detail.html"
                    class="uk-position-cover c-transparent"
                  >
                    details de l'évenement
                  </a>
                </div>
              </div>
              <div class="mt-30">
                <div
                  class="uk-card uk-card-small uk-card-default uk-card-hover uk-border-rounded-large uk-overflow-hidden"
                >
                  <div class="uk-card-media-top uk-inline uk-light">
                    <img
                      src="https://www.la-corse-autrement.com/wp-content/uploads/2021/07/DJI_0408-1128x484.jpg"
                      alt="image de l'évenement"
                      class="max-height-154 media-500-max-width-173 object-fit-cover"
                    />
                    <div class="uk-position-cover uk-overlay-xlight"></div>
                    <div class="uk-position-small uk-position-top-left">
                      <span class="uk-label uk-text-bold uk-text-price">
                        10/07/2023
                      </span>
                    </div>
                    <div
                      class="uk-position-small uk-position-top-right card-icon-bgc"
                    >
                      <i
                        class="fa-regular fa-heart cursor-pointer uk-icon-button uk-like uk-position-z-index uk-position-relative card-icon-bg-none"
                      ></i>
                    </div>
                  </div>
                  <div class="uk-card-body">
                    <h3 class="uk-card-title uk-margin-small-bottom">Titre</h3>
                    <div class="uk-text-muted uk-text-small">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Ratione expedita quaerat laboriosam soluta eligendi nemo.
                    </div>
                    <div
                      class="uk-text-muted uk-text-xxsmall uk-rating uk-margin-small-top"
                    >
                      <i class="fa-regular fa-star"></i>
                      <i class="fa-regular fa-star"></i>
                      <i class="fa-regular fa-star"></i>
                      <i class="fa-regular fa-star"></i>
                      <i class="fa-regular fa-star"></i>
                    </div>
                  </div>
                  <a
                    href="./product-detail.html"
                    class="uk-position-cover c-transparent"
                  >
                    details de l'évenement
                  </a>
                </div>
              </div>
              <div class="mt-30">
                <div
                  class="uk-card uk-card-small uk-card-default uk-card-hover uk-border-rounded-large uk-overflow-hidden"
                >
                  <div class="uk-card-media-top uk-inline uk-light">
                    <img
                      src="https://www.la-corse-autrement.com/wp-content/uploads/2021/07/DJI_0408-1128x484.jpg"
                      alt="image de l'évenement"
                      class="max-height-154 media-500-max-width-173 object-fit-cover"
                    />
                    <div class="uk-position-cover uk-overlay-xlight"></div>
                    <div class="uk-position-small uk-position-top-left">
                      <span class="uk-label uk-text-bold uk-text-price">
                        10/07/2023
                      </span>
                    </div>
                    <div
                      class="uk-position-small uk-position-top-right card-icon-bgc"
                    >
                      <i
                        class="fa-regular fa-heart cursor-pointer uk-icon-button uk-like uk-position-z-index uk-position-relative card-icon-bg-none"
                      ></i>
                    </div>
                  </div>
                  <div class="uk-card-body">
                    <h3 class="uk-card-title uk-margin-small-bottom">Titre</h3>
                    <div class="uk-text-muted uk-text-small">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Ratione expedita quaerat laboriosam soluta eligendi nemo.
                    </div>
                    <div
                      class="uk-text-muted uk-text-xxsmall uk-rating uk-margin-small-top"
                    >
                      <i class="fa-regular fa-star"></i>
                      <i class="fa-regular fa-star"></i>
                      <i class="fa-regular fa-star"></i>
                      <i class="fa-regular fa-star"></i>
                      <i class="fa-regular fa-star"></i>
                    </div>
                  </div>
                  <a
                    href="./product-detail.html"
                    class="uk-position-cover c-transparent"
                  >
                    details de l'évenement
                  </a>
                </div>
              </div>
              <div class="mt-30">
                <div
                  class="uk-card uk-card-small uk-card-default uk-card-hover uk-border-rounded-large uk-overflow-hidden"
                >
                  <div class="uk-card-media-top uk-inline uk-light">
                    <img
                      src="https://www.la-corse-autrement.com/wp-content/uploads/2021/07/DJI_0408-1128x484.jpg"
                      alt="image de l'évenement"
                      class="max-height-154 media-500-max-width-173 object-fit-cover"
                    />
                    <div class="uk-position-cover uk-overlay-xlight"></div>
                    <div class="uk-position-small uk-position-top-left">
                      <span class="uk-label uk-text-bold uk-text-price">
                        10/07/2023
                      </span>
                    </div>
                    <div
                      class="uk-position-small uk-position-top-right card-icon-bgc"
                    >
                      <i
                        class="fa-regular fa-heart cursor-pointer uk-icon-button uk-like uk-position-z-index uk-position-relative card-icon-bg-none"
                      ></i>
                    </div>
                  </div>
                  <div class="uk-card-body">
                    <h3 class="uk-card-title uk-margin-small-bottom">Titre</h3>
                    <div class="uk-text-muted uk-text-small">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Ratione expedita quaerat laboriosam soluta eligendi nemo.
                    </div>
                    <div
                      class="uk-text-muted uk-text-xxsmall uk-rating uk-margin-small-top"
                    >
                      <i class="fa-regular fa-star"></i>
                      <i class="fa-regular fa-star"></i>
                      <i class="fa-regular fa-star"></i>
                      <i class="fa-regular fa-star"></i>
                      <i class="fa-regular fa-star"></i>
                    </div>
                  </div>
                  <a
                    href="./product-detail.html"
                    class="uk-position-cover c-transparent"
                  >
                    details de l'évenement
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
