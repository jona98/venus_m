<!DOCTYPE html>
<html lang="fr">

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Accueil</title>
  <!-- Font Awesome Icons -->

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" />
  <!-- Stylesheet -->
  <link rel="stylesheet" href="{{ asset ('venus_css/style.css')}}" />
  <link rel="stylesheet" href="{{ asset ('venus_css/header.css')}}" />
  <link rel="stylesheet" href="{{ asset ('venus_css/globals.css')}}" />
  <link rel="stylesheet" href="{{ asset ('venus_css/splide.min.css')}}" />
  <style>
    .cart-icon {
      position: fixed;
      bottom: 20px;
      right: 20px;
      background-color: #63016e;
      color: #fff;
      width: 40px;
      height: 40px;
      border-radius: 50%;
      text-align: center;
      line-height: 40px;
      cursor: pointer;
    }

    .cart-icon i {
      font-size: 20px;
    }

    .cart-count {
      position: absolute;
      top: 0;
      right: 0;
      background-color: #e74c3c;
      color: #fff;
      width: 20px;
      height: 20px;
      border-radius: 50%;
      font-size: 12px;
      line-height: 20px;
    }
  </style>
</head>

<body style="background-color: #f9f8fb">

  <header>
    <nav>
      <a href="/" id="logo">
        <img src="{{ asset ('venus_images/logo-nv2_1-removebg-preview.png')}}" alt="logo" />
      </a>
      <i class="fas fa-bars" id="ham-menu" onclick="toggleMenu()"></i>
      <ul id="nav-bar">
        <li>
          <a href="/" class="nav-link active">Accueil</a>
        </li>
        <li>
          <a href="{{route('produit.index')}}" class="nav-link active">Nos produits</a>
        </li>
        <li>
          <i class="fa-solid fa-cart-shopping"></i>
          <a href="{{route('cart.checkout')}}" class="nav-link">Panier ({{ session('cart') ? count(session('cart')) : 0 }})</a>
        </li>
        @if (Auth::check())
        <li>
          <i class="fa-regular fa-heart"></i>
          <a href="{{url('/favorite')}}" class="nav-link">Mes Favories</a>
        </li>
        <li id="profile-dropdown" class="dropdown">
          <div class="dropbtn" onclick="dropdownFunction()">
            <i class="fas fa-user dropbtn"></i>
            <span class="dropbtn" style="font-weight: bold">{{ Auth::user()->pseudo }}</span>
            <i class="fas fa-caret-down dropbtn"></i>
          </div>
          <div class="dropdown-content" id="myDropdown">
            <a href="/profile/identity" class="personal-link d-block mt-4px mb-4px">Mon profil</a>
            <a href="/profile/identity" class="personal-link d-block mt-4px mb-4px">Mes commandes</a>
            <a style="background-color: #63016e; color: #fff" href="{{ route('user.auth.logout') }}">
              Déconnexion
            </a>
          </div>
        </li>
        @else
        <li>
          <a href="{{route('user.login')}}" class="nav-link login-button uk-button">Se Connecter</a>
        </li>
        @endif
      </ul>
    </nav>
    <div class="next-header-container">
      <ul class="menu next-header">
        @if(isset($produitCategories))
        @foreach($produitCategories as $produitCategory)
        <li class="categorie">
          <a href="{{route('produit.byCategory',$produitCategory->id)}}">{{$produitCategory->titre}}</a>
        </li>
        @endforeach
        @endif
      </ul>
    </div>
    <div id="modal" class="modal" style="display: none">
      <div class="modal-contents">
        <div class="modal-image-wrapper">
          <span id="closeModal" class="close">
            <i class="fa-regular fa-circle-xmark"></i>
          </span>
        </div>
        <div class="modal-content">
          <div class="mt-20 mb-25 txt-align-center">
            <h3 class="m-0 mb-25">Que voulez vous publier?</h3>
            <a href="/multi-step-form.html" id="offerLink" class="offer-request-link offer-link mb-20">Une offre</a>
            <a href="#" id="requestLink" class="offer-request-link request-link">Une demande</a>
          </div>
        </div>
      </div>
    </div>
  </header>


  <section class="hero-block">
    <div class="hero-block__background">
      <div class="web_ui__Image__image web_ui__Image__cover web_ui__Image__scaled">
        <img src="{{asset('venus_images/img.jpg')}}" alt="Banion de la page d'accueil" class="web_ui__Image__content" />
      </div>
      <div class="hero-block__overlay"></div>
    </div>
    <h1 class="mb-50 uk-position-z-index" style="
          text-align: center;
          color: #fff;
          text-shadow: 2px 2px 4px rgb(0 0 0 / 100%);
        ">
      Our Concern Is you
    </h1>
    <div class="uk-container z-index3 position-rel max-width-1024">
      <div class="uk-background-secondary uk-background-purple uk-light uk-border-rounded-large uk-header-banner uk-header-banner-events uk-position-relative uk-position-z-index uk-box-shadow-small mt-0" data-uk-scrollspy="cls: uk-animation-scale-up; repeat: true" <form class="uk-grid-small search-bar-form al-item-center" data-uk-grid>
        <div class="uk-width-expand">
          <div class="uk-child-width-1-3@m uk-grid" data-uk-grid>
            <div>
              <label class="uk-form-label font-weight400" style="color:#fff">Nom</label>
              <input id="name" name="name" class="uk-input font-weight400 c-black" type="text" placeholder="Nom du produit." autocomplete="off" />
            </div>
            <div>
              <label class="uk-form-label font-weight400" style="color:#fff">Catégorie</label>
              <select class="uk-select font-weight400 c-black" name="categorie" id="categorie">
                <option value="" class="font-weight400" style="color:#fff">
                  Catégorie...
                </option>
                @if(isset($produitCategories))
                @foreach($produitCategories as $produitCategory)
                <option value="" class="font-weight400" style="color:#fff">{{$produitCategory->titre}}</option>
                @endforeach
                @endif
              </select>
            </div>
          </div>
        </div>
        <div class="uk-width-auto uk-flex uk-flex-bottom">
          <button type="submit" class="uk-icon-button mt-20 m-left-15 bgc-orange cursor-pointer" title="Rechercher">
            <i class="fa-solid fa-magnifying-glass" style="font-size: 20px"></i>
          </button>
        </div>
        </form>
      </div>
    </div>
  </section>
  <section class="uk-section pb-0">
    <div class="uk-container">
      <div class="uk-grid-small uk-flex uk-flex-middle uk-grid">
        <div class="uk-width-expand@m uk-first-column">
          <h2>Top catégories</h2>
        </div>
      </div>
      <div class="uk-child-width-1-2@s uk-child-width-1-3@m uk-grid-match uk-margin-medium-top uk-grid">
        <div style="width: 100%; overflow: hidden">
          <div id="carousel1" class="splide" data-splide='{  "perMove": 1, "type": "loop", "rewind": true, "keyboard": true, "gap": "1rem", "pagination": false, "padding": "2rem" }'>
            <div class="splide__track">
              <ul class="splide__list">
                @if(isset($produitCategories))
                @foreach($produitCategories as $produitCategory)
                @if($produitCategory->sous_categories !== null)
                @foreach($produitCategory->sous_categories as $sousCategorie)
                <li class="splide__slide">
                  <div class="category-card-container"><a href="{{route('produit.bySousCategory',$sousCategorie->id)}}">{{$sousCategorie->titre}}</a></div>
                </li>
                @endforeach
                @endif
                @endforeach
                @endif
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="uk-section">
    <div class="uk-container">
      <div class="uk-grid-small uk-flex uk-flex-middle uk-grid">
        <div class="uk-width-expand@m uk-first-column">
          <h2>Produits récents</h2>
        </div>
      </div>
      <div class="uk-child-width-1-2@s uk-child-width-1-3@m uk-grid-match uk-margin-medium-top uk-grid">
        <div style="width: 100%; overflow: hidden">
          <div id="carousel2" class="splide" data-splide='{ "perMove": 1, "type": "loop", "rewind": true, "keyboard": true, "gap": "1rem", "pagination": false, "padding": "2rem" }'>
            <div class="splide__track">
              <ul class="splide__list">
                @if(isset($latestProduits))
                @foreach($latestProduits as $latestProduit)
                <li class="splide__slide">
                  <div class="uk-card uk-card-small uk-card-default uk-card-hover uk-border-rounded-large uk-overflow-hidden">
                    <div class="uk-card-media-top uk-inline uk-light">
                      <img src="{{ 'https://api.venusforyoung.com' . $latestProduit->cover }}" alt="{{$latestProduit->titre}}" width="500" height="200" loading="eager" class="max-height-132 object-fit-cover" />
                      <div class="uk-position-cover uk-overlay-xlight"></div>
                      <div class="uk-position-small uk-position-top-left">
                        <span class="uk-label uk-text-bold uk-text-price">{{$latestProduit->prix}} XOF</span>
                      </div>
                      <div class="uk-position-small uk-position-top-right card-icon-bgc">
                        <i class="fa-regular fa-heart"></i>
                      </div>
                    </div>
                    <div class="uk-card-body" style="padding: 15px 10px">
                      <h3 class="uk-card-title uk-margin-small-bottom">
                        <a href="{{route('produit.detail',$latestProduit->id)}}" style="color: #333;">{{ Str::limit($latestProduit->titre, 100) }}</a>
                      </h3>
                      <div class="uk-text-muted uk-text-small">
                        {{ Str::limit($latestProduit->description, 100, '...') }}
                      </div>
                      <div class="uk-text-muted uk-text-xxsmall uk-rating uk-margin-small-top">
                        <i class="fa-regular fa-star mr-2 cursor-pointer" style="color: #f6ba00"></i>
                        <i class="fa-regular fa-star mr-2 cursor-pointer" style="color: #f6ba00"></i>
                        <i class="fa-regular fa-star mr-2 cursor-pointer" style="color: #f6ba00"></i>
                        <i class="fa-regular fa-star mr-2 cursor-pointer" style="color: #f6ba00"></i>
                        <i class="fa-regular fa-star mr-2 cursor-pointer" style="color: #f6ba00"></i>
                      </div>
                    </div>
                    <a href="{{route('produit.detail',$latestProduit->id)}}" class="uk-position-cover c-transparent">Voir plus</a>
                  </div>
                </li>
                @endforeach
                @endif
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="uk-section uk-margin-top pt-0">
    <div class="uk-container">
      <div class="uk-grid-small uk-flex uk-flex-middle uk-grid">
        <div class="uk-width-expand@m uk-first-column">
          <h2>Nos produits</h2>
        </div>
        <div class="uk-width-auto@m mt-20-media-950">
          <a href="{{route('produit.index')}}" class="btn btn-warning" style="background-color: #63016e; color: #fff">Tout voir</a>
        </div>
      </div>
      <div class="uk-child-width-1-2@s uk-child-width-1-3@m uk-grid-match uk-margin-medium-top uk-grid">
        <div style="width: 100%; overflow: hidden">
          <div class="uk-child-width-1-2@s uk-child-width-1-3@m uk-child-width-1-4@l uk-grid-match uk-grid mt-0">
            <!-- card -->
            @if(isset($produits))
            @foreach($produits as $produit)
            <div class="mt-30">
              <div class="uk-card uk-card-small uk-card-default uk-card-hover uk-border-rounded-large uk-overflow-hidden">
                <div class="uk-card-media-top uk-inline uk-light">
                  <img src="{{ 'https://api.venusforyoung.com' . $produit->cover }}" alt="image de l'évenement" class="max-height-154 media-500-max-width-173 object-fit-cover" />
                  <div class="uk-position-cover uk-overlay-xlight"></div>
                  <div class="uk-position-small uk-position-top-left">
                    <span class="uk-label uk-text-bold uk-text-price">
                      {{$produit->prix}} XOF
                    </span>
                  </div>
                  <div class="uk-position-small uk-position-top-right card-icon-bgc">
                    <i class="fa-regular fa-heart cursor-pointer uk-icon-button uk-like uk-position-z-index uk-position-relative card-icon-bg-none"></i>
                  </div>
                </div>
                <div class="uk-card-body">
                  <h3 class="uk-card-title uk-margin-small-bottom"><a href="{{route('produit.detail',$produit->id)}}" style="color: #333;">{{ Str::limit($produit->titre, 100) }}</a></h3>
                  <div class="uk-text-muted uk-text-small">
                    {{ Str::limit($produit->description, 150, '...') }}
                  </div>
                  <div class="uk-text-muted uk-text-xxsmall uk-rating uk-margin-small-top">
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                  </div>
                </div>
                <a href="{{route('produit.detail',$produit->id)}}" class="uk-position-cover c-transparent">
                  Voir plus
                </a>
              </div>
            </div>
            @endforeach
            @endif
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="cart-icon" onclick="window.location.href='{{ route('cart.checkout')}}'">
    <i class="fa fa-shopping-cart"></i>
    <span class="cart-count">{{ session('cart') ? count(session('cart')) : 0 }}</span>
  </div>
  <section>
    <footer class="uk-section uk-section-large">
      <div class="uk-container uk-text-muted">
        <div class="uk-child-width-1-2@s uk-child-width-1-5@m uk-grid" data-uk-grid style="justify-content: space-around">
          <div class="mt-20-media-639">
            <div class="uk-margin">
              <h2>
                <a href="/" class="uk-logo" style="color: #fff;"> Venus </a>
              </h2>
            </div>
            <div class="uk-margin uk-text-small">
              <p class="ft-size16" style="color: #fff;">
                Venus.
              </p>
            </div>
            <div class="uk-margin">
              <div data-uk-grid class="uk-child-width-auto uk-grid-small d-flex">
                <div class="uk-first-column">
                  <a href="https://www.facebook.com/" class="uk-icon-link uk-icon" target="_blank" rel="noreferrer" title="Facebook">
                    <i class="fab fa-facebook" style="font-size: 26px; color: #fff"></i>
                  </a>
                </div>
                <div>
                  <a href="https://www.instagram.com/" class="uk-icon-link uk-icon" target="_blank" rel="noreferrer" title="Instagram">
                    <i class="fab fa-instagram" style="font-size: 26px; color: #fff"></i>
                  </a>
                </div>
                <div>
                  <a href="#" class="uk-icon-link uk-icon" target="_blank" rel="noreferrer" title="Email">
                    <i class="far fa-envelope" style="font-size: 26px; color: #fff"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div>
            <h2 style="color: #fff;">Liens Utiles</h2>
            <ul class="uk-list uk-text-small">
              <li>
                <a class="uk-link-muted" href="/" style="color: #fff;">Accueil</a>
              </li>
              <li>
                <a class="uk-link-muted" href="/about" style="color: #fff;">Collaboration</a>
              </li>
              <li>
                <a class="uk-link-muted" href="/about" style="color: #fff;">Nos conditions d'utilisation (CGU)</a>
              </li>
            </ul>
          </div>
          <div>
            <h2 style="color: #fff;">Obtenir L'application</h2>
            <ul class="uk-list uk-text-small">
              <li>
                <a class="uk-link-muted" href="#">
                  <img class="nav-img" style="width: 100%; height: auto; color: #fff" src="./assets/Google_Play_Store.png" alt="logo" priority loading="eager" />
                  Télécharger sur Play Store
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
    <div class="footer-hilexpertiz">
      <p class="m-auto txt-align-center">
        Venus ©2024 All rights reserved | Développé par
        <a href="https://hilexpertiz.africa" target="_blank" rel="noreferrer">Juste Innover</a>
      </p>
    </div>
  </section>

  <!-- Script -->
  <script src="{{ asset ('venus_js/script.js')}}"></script>

  <script src="{{ asset ('venus_js/splide.min.js')}}"></script>
  <script src="{{ asset ('venus_js/splide.js')}}"></script>
</body>

</html>