<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommandeProduit extends Model
{
    use HasFactory;

    protected $fillable = [
        'code',
        'prixTotal',
        'date',
        'status',
        'user_id',
    ];

    protected $with = [
        'livraison',
        'transaction',
        'articles',
        'user'
    ];
    public function livraison()
    {
        return $this->hasMany(Livraison::class, 'commande_produit_id');
    }
    public function transaction()
    {
        return $this->hasMany(Transaction::class, 'commande_produit_id');
    }
    public function articles()
    {
        return $this->hasMany(ArticlesProduit::class, 'commande_produit_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
