<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategorieProduit extends Model
{
    use HasFactory;

    protected $with = [
        'sous_categories',
    ];

    public function sous_categories()
    {
        return $this->hasMany(SousCategorieProduit::class, 'categorie_produit_id');
    }
}
