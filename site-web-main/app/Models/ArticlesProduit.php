<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArticlesProduit extends Model
{
    use HasFactory;

    protected $fillable = [
        'prix_unitaire',
        'quantite',
        'commande_produit_id',
        'produit_id',
    ];

    protected $with = [
        'produits',

    ];

    public function produits()
    {
        return $this->belongsTo(Produit::class, 'produit_id');
    }
}
