<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'numero_transaction',
        'montant',
        'date_paiement',
        'status',
        'commande_produit_id',
        'mode_paiement_id',
        'user_id',
        'commentaire',
    ];

    const ORDER = "Commande";
    const RECHARGE = "Recharge";

    protected $with = [
        'mode_paiement'

    ];

    public function mode_paiement()
    {
        return $this->belongsTo(ModePaiement::class, 'mode_paiement_id');
    }
}
