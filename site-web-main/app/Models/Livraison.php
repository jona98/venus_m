<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Livraison extends Model
{
    use HasFactory;

    protected $fillable = [
        'telephone',
        'status',
        'adresse',
        'date_livraison',
        'commande_produit_id',
        'mode_livraison_id',
        'description',
        'longitude',
        'latitude',
        'user_id'
    ];


    protected $with = [
        'mode_livraison',

    ];

    public function mode_livraison()
    {
        return $this->belongsTo(ModeLivraison::class, 'mode_livraison_id');
    }
}
