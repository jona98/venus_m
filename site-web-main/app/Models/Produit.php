<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produit extends Model
{
    use HasFactory;

    protected $with = [
        'sous_categorie',
        'images',
    ];


    public function images()
    {
        return $this->hasMany(ProduitImage::class, 'produit_id');
    }
    public function sous_categorie()
    {
        return $this->belongsTo(SousCategorieProduit::class, 'sous_categorie_produit_id');
    }
}
