<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'pseudo',
        'telephone',
        'password',
        'naissance',
        'role_id',
        'genre_id',
        'is_blocked',
        'image',
        'first_name',
        'last_name',
        'heure_disponible_matin',
        'heure_disponible_soir',
        'pays_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array<int, string>
     */
    protected $appends = [
        'profile_photo_url',
    ];

    const PHONE_HEADER_TG = "99|98|97|96|90|91|92|93|70|79|71";
    //const PHONE_HEADER_GH = "20|23|24|26|27|28|54|57";
    const PHONE_HEADER_GH = "20|23|24|25|26|27|28|50|54|55|56|57|59";
    const PHONE_HEADER_SN = "70|76|77|78";
    const PHONE_HEADER_CI = "01|05|07";
    const PHONE_HEADER_BJ = "90|98|51|99|52|95|61|53|97|62|54|91|63|55|64|94|65|96|66|67";
    const PHONE_HEADER_BF = "01|70|56|76|02|71|57|77|51|72|58|68|52|73|64|69|53|05|65|78|60|06|66|79|61|07|67|62|54|74|63|55|75";
    const PHONE_HEADER_GB = "62|66|65|74|77";
    const PHONE_HEADER_FR = "601|602|603|604|605|6026|607|608|609|610|611|612|613|614|615|617|618|619|620|628|629|630|631|632|633|634|635|636|637|638|
    |640|641|642|643|644|645|646|647|648|649|650|652|656|657|658|659|660|661|662|663|664|665|666|667|668|669|670|671|672|673|674|675|676|677|678|679|680|
    681|682|683|684|685|686|687|688|689|695|698|699|730||731|732|733|734|735|736|737|738|739|740|741|742|743|744|745|746|747|748|749|750|751|752
|753|754|755|756|757|758|759|760|761|762|763|764|765|767|768|769|770|771|772|773|774|775|776|777|778|779|780|781|782|783|784|785|788|788|789";

    const PHONE_HEADER = self::PHONE_HEADER_TG . self::PHONE_HEADER_GH . self::PHONE_HEADER_SN . self::PHONE_HEADER_CI . self::PHONE_HEADER_BJ . self::PHONE_HEADER_BF .  self::PHONE_HEADER_GB . self::PHONE_HEADER_FR;

    /**
     * Get the attributes that should be cast.
     *
     * @return array<string, string>
     */
    protected function casts(): array
    {
        return [
            'email_verified_at' => 'datetime',
            'password' => 'hashed',
        ];
    }
}
