<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SousCategorieProduit extends Model
{
    use HasFactory;

    public function produit_category()
    {
        return $this->belongsTo(CategorieProduit::class, 'categorie_produit_id');
    }
}
