<?php

namespace App\Http\Controllers;

use App\Models\CategorieProduit;
use App\Models\Produit;
use App\Models\SousCategorieProduit;
use Illuminate\Http\Request;

class ProduitController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $produitCategories = CategorieProduit::orderby("titre", "asc")->get();
        $produits = Produit::orderBy('created_at', 'asc')->paginate(12);
        return view('produits', compact('produits', 'produitCategories'));
    }

    public function produitByCategory(CategorieProduit $categoryProduit)
    {
        $produitCategories = CategorieProduit::orderby("titre", "asc")->get();
        $produits = Produit::whereIn('sous_categorie_produit_id', $categoryProduit->sous_categories->pluck('id'))->paginate(12);
        return view('produit-by-category', compact('produits', 'produitCategories'));
    }

    public function getProduitBySousCategory(SousCategorieProduit $sousCategoryProduit)
    {
        $produitCategories = CategorieProduit::orderby("titre", "asc")->get();
        $produits = Produit::where('sous_categorie_produit_id', $sousCategoryProduit)->paginate(12);
        return view('produit-by-sous-category', compact('produits', 'produitCategories'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Produit $produit)
    {
        $produitCategories = CategorieProduit::orderby("titre", "asc")->get();

        return view('product-detail', compact('produit', 'produitCategories'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Produit $produit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Produit $produit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Produit $produit)
    {
        //
    }
}
