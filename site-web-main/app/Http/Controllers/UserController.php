<?php

namespace App\Http\Controllers;

use App\Models\CategorieProduit;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function dashboard()
    {
        $produitCategories = CategorieProduit::orderby("titre", "asc")->get();
        return view("user.dashboard", compact('produitCategories'));
    }
}
