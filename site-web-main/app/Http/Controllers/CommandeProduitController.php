<?php

namespace App\Http\Controllers;

use App\Models\ArticlesProduit;
use App\Models\CommandeProduit;
use App\Models\Livraison;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CommandeProduitController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        try {
            $validation = Validator::make($request->all(), [
                'prix_total' => 'required|numeric',
                'transaction_mode_paiement_id' => 'required|integer|min:1|exists:mode_paiements,id',
                'livraison_adresse' => 'required|string',
                'livraison_mode_livraison_id' => 'required|integer|min:1|exists:mode_livraisons,id'
            ]);
            //on envoie une erreur 422 si les données ne sont pas validées
            if ($validation->fails()) {
                session()->flash('error', $validation->errors()->first());
                return redirect()->back();
            }

            $articleProduits = "";
            $cart = session()->get('cart');
            if (!$cart) {
                session()->flash('error', "Panier vide");
                return redirect()->back();
            } else {
                $articleProduits = $cart;
            }

            /* $user = User::find($request->input("user_id"));
            $porteMonnaie = PorteMonnaie::where('user_id', $user->id)->first();
            if (!$porteMonnaie) {
                $porteMonnaie = new PorteMonnaie();
                $porteMonnaie->user_id=$user->id;
            }
            */

            // $validator = Validator::make($request->all(), $rules);


            // if ($validator->fails()) {
            //     return response()->json(['message' => $validator->errors(), 'error' => true], 422);
            // }

            // Utilisez les données de la requête
            $commande_produit['prixTotal'] = $request->prix_total;
            $commande_produit['date'] = now();
            $commande_produit['user_id'] =  Auth::user()->id;
            $code = Str::random(6);
            $code = preg_replace('/[^A-Za-z0-9]/', '', $code);
            $code = strtoupper($code);
            $commande_produit['code'] =  $code;
            $commande_produit['status'] = "en cours";

            $codeT = Str::random(10);
            $codeT = preg_replace('/[^A-Za-z0-9]/', '', $codeT);
            $codeT = strtoupper($codeT);
            $transaction['numero_transaction'] =  $codeT;
            $transaction['date_paiement'] = now();
            $transaction['montant'] = $request->prix_total;
            $transaction['user_id'] = Auth::user()->id;
            $transaction['status'] = "en cours";
            $transaction['mode_paiement_id'] = $request->transaction_mode_paiement_id;

            $livraison['telephone'] = Auth::user()->telephone;
            $livraison['user_id'] = Auth::user()->id;
            $livraison['status'] = "en cours";
            $livraison['adresse'] = $request->livraison_adresse;
            $livraison['date_livraison'] = now();
            $livraison['mode_livraison_id'] = $request->livraison_mode_livraison_id;
            $livraison['longitude'] = "null";
            $livraison['latitude'] = "null";
            $livraison['description'] = "null";

            // Start transaction!
            DB::beginTransaction();
            try {
                $commande_produit = CommandeProduit::create($commande_produit);
            } catch (\Throwable $th) {
                DB::rollback();
                Log::info("COMMANDE PRODUIT - Commande : " . $th->getMessage());
                return response()->json(
                    ['message' => "Une erreur s'est produite veuillez réessayer SVP", 'error' => true],
                    500
                );
            }

            try {
                $transaction['commande_produit_id'] = $commande_produit->id;
                $transaction = Transaction::create($transaction);
            } catch (\Throwable $th) {
                DB::rollback();
                Log::info("COMMANDE PRODUIT - Transaction : " . $th->getMessage());
                return response()->json(
                    ['message' => "Une erreur s'est produite veuillez réessayer SVP", 'error' => true],
                    500
                );
            }

            try {
                $livraison['commande_produit_id'] = $commande_produit->id;
                $livraison['user_id'] = Auth::user()->id;
                $livraison = Livraison::create($livraison);
            } catch (\Throwable $th) {
                DB::rollback();
                Log::info("COMMANDE PRODUIT - Livraison : " . $th->getMessage());
                return response()->json(
                    ['message' => "Une erreur s'est produite veuillez réessayer SVP", 'error' => true],
                    500
                );
            }

            try {
                if ($articleProduits) {
                    foreach ($articleProduits as $articleProduit) {
                        ArticlesProduit::create([
                            'prix_unitaire' => $articleProduit['prix'],
                            'quantite' => $articleProduit['quantity'],
                            'commande_produit_id' => $commande_produit->id,
                            'produit_id' => $articleProduit['produit_id']
                        ]);
                    }
                }
                /*
                $porteMonnaie->montant -= $request->montant;
                $porteMonnaie->save();
                */
            } catch (\Throwable $th) {
                DB::rollback();
                Log::info("COMMANDE PRODUIT - Article : " . $th->getMessage());
                return response()->json(
                    ['message' => "Une erreur s'est produite veuillez réessayer SVP", 'error' => true],
                    500
                );
            }

            // If we reach here, then
            // data is valid and working.
            // Commit the queries!
            DB::commit();

            $CinetPayController = new CinetPayController();
            return $CinetPayController->getFormSignature($commande_produit);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'error' => true], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(CommandeProduit $commandeProduit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(CommandeProduit $commandeProduit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, CommandeProduit $commandeProduit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(CommandeProduit $commandeProduit)
    {
        //
    }
}
