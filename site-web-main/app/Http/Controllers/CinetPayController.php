<?php

namespace App\Http\Controllers;

use App\Models\CommandeProduit;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Session;

class CinetPayController extends Controller
{
    public function getFormSignature($request)
    {
        try {

            $user = User::find($request->user_id);
            $commande_produit = CommandeProduit::where('id', $request->id)
                ->where('user_id', $user->id)->first();


            //create order 
            $apiGfsURL = 'https://api-checkout.cinetpay.com/v2/payment';
            $notify_url = 'https://api.venusforyoung.com/api/callback-cinetpay';
            $return_url = 'https://venusforyoung.com/paiement-return';

            //Préparation transaction id
            $data = [];
            $data['apikey'] = env('CINETPAY_API_KEY');
            $data['site_id'] = env('CINETPAY_SITE_ID');
            $data['currency'] = "XOF";
            $data['description'] = "Paiement d'une commande";
            $data['channels'] = 'ALL';
            $data['notify_url'] = $notify_url;
            $data['return_url'] = $return_url;
            $data['amount'] =  intval($commande_produit->prixTotal);

            $transaction = Transaction::where('user_id', $user->id)->where('commande_produit_id', $commande_produit->id)->first();

            if ($transaction) {
                $transaction->type = Transaction::ORDER;
                $transaction->cpm_trans_id = $transaction->numero_transaction;
                $transaction->cpm_amount = intval($data['amount']);
                $transaction->type_paiement = 'CINETPAY';
                $transaction->created_at = now();
                $transaction->save();
            }

            $transaction = Transaction::where('user_id', $user->id)->where('commande_produit_id', $commande_produit->id)->first();
            $data['transaction_id'] = $transaction->cpm_trans_id;

            $response = Http::withHeaders([
                'Content-Type' => 'application/json',
            ])->withBody(json_encode($data), 'application/json')
                ->post($apiGfsURL);

            $responseArray = json_decode($response, true);

            if ($response['code'] == "201") {
                Transaction::where('id', $transaction->id)->update([
                    'signature' => $response['data']['payment_token']
                ]);

                return redirect()->away($response['data']['payment_url']);
            } else {
                Session::flash('error', 'Une erreur s\'est produite. Veuillez réessayer SVP!');
            }
            // $data['signature'] = $responseJson;
            // $response = Http::post($apiSfwsURL, $data);

            // return $response;
        } catch (\Exception $e) {
            DB::rollback();
            Log::info("CINETPAY SIGNATURE ERROR : " . $e->getMessage());
            Session::flash('error', 'Une erreur s\'est produite. Veuillez réessayer SVP!');
        }
    }


    public function paiementReturn(Request $request)
    {
        $data = $request->only(
            'token'
        );

        // return response()->json(['error' => 'damn']);
        $check = DB::table('transactions')->where('signature', $data['token'])->count();

        if ($check == 1) {
            $transaction = DB::table('transactions')->where('signature', $data['token'])->first();

            if ($transaction) {
                $apiURL = 'https://api-checkout.cinetpay.com/v2/payment/check';
                $data = [
                    'apikey' => env('CINETPAY_API_KEY'),
                    'site_id' => env('CINETPAY_SITE_ID'),
                    'token' => $transaction->signature,
                ];
                $response = Http::post($apiURL, $data);

                if ($response['code'] == "00") {
                    return view('paiement', ['message' => 'Paiement effectué avec succès. Veuillez consulter vos mails.']);
                } else {
                    return view('paiement', ['message' => $response['message']]);
                }
            }
        }
    }

    public function formError($error)
    {
        return view('error', ['error' => $error]);
    }
}
