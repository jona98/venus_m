<?php

namespace App\Http\Controllers;

use App\Models\CommandeProduit;
use App\Models\ModeLivraison;
use App\Models\ModePaiement;
use Illuminate\Database\Events\TransactionBeginning;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;

class CartController extends Controller
{
    public function checkout()
    {
        $ModePaiements = ModePaiement::all();
        $ModeLivraisons = ModeLivraison::all();
        return view("checkout", compact('ModePaiements', 'ModeLivraisons'));
    }

    public function addItem(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'produit_name' => 'required|string',
                'produit_id' => 'required|exists:produits,id',
                'quantity' => 'required|integer|min:1',
                'prix' => 'required|numeric|min:0'
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $produitName = $request->input('produit_name');
            $produitId = $request->input('produit_id');
            $quantity = $request->input('quantity');
            $prix = $request->input('prix');

            $cart = session()->get('cart');

            // Si le panier est vide, on initialise un tableau vide
            if (!$cart) {
                $cart = [];
            }

            // Si le produit est déjà dans le panier, on met à jour la quantité
            if (isset($cart[$produitId])) {
                $cart[$produitId]['quantity'] += $quantity;
            } else {
                // Sinon, on ajoute le produit au panier
                $cart[$produitId] = [
                    'produit_id' => $produitId,
                    'produit_name' => $produitName,
                    'quantity' => $quantity,
                    'prix' => $prix
                ];
            }

            // Mettre à jour la session du panier
            session()->put('cart', $cart);

            // return redirect()->back()->with('success', 'Produit ajouté au panier.');
            return redirect()->route('produit.detail', $produitId)->with('success', 'Produit ajouté au panier.');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', $th->getMessage());
        }
    }
}
