<?php

namespace App\Http\Controllers;

use App\Models\CategorieProduit;
use App\Models\Produit;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function landingPage()
    {
        $produitCategories = CategorieProduit::orderby("titre", "asc")->get();
        $produits = Produit::orderBy('created_at', 'asc')->paginate(12);
        $latestProduits = Produit::latest()->take(6)->get();
        return view('index', compact('produits', 'produitCategories', 'latestProduits'));
    }
}
