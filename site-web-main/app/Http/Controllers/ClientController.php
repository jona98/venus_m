<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redirect;

class ClientController extends Controller
{
    public function NewClient(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'password' => 'required',
            'password_confirmation' => 'required|same:password',
        ]);

        $client = new Client();
        $client->name = $request->name;
        $client->phone = $request->phone;
        $client->password = bcrypt($request->password);
        $client->save();

        return Redirect::back()->with('status', 'Votre inscription a été validée avec succès.');
    }
}

