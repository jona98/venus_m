<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Fortify\Fortify;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'telephone' => 'required|numeric|min:8',
                'password' => 'required|string',
            ]
        );

        //on envoie une erreur 422 si les données ne sont pas validées
        if ($validator->fails()) {
            session()->flash('error', $validator->errors()->first());
            return redirect()->back();
        }

        $phone = $request->input('telephone');
        $password = $request->input('password');

        // Vérifie si l'utilisateur existe
        $user = User::where('telephone', $phone)->first();
        //  dd($user->password);


        // Si l'utilisateur n'existe pas, renvoie un message d'erreur
        if ($user == null) {
            session()->flash('error', 'L\'utilisateur n\'existe pas. ');
            return redirect()->back();
        }
        if (!$user) {
            session()->flash('error', 'L\'utilisateur n\'existe pas. ');
            return redirect()->back();
        }

        // Vérifie si le mot de passe est correct
        if (!Hash::check($password, $user->password)) {
            session()->flash('error', 'Le mot de passe est incorrect. ');
            return redirect()->back();
        }

        // Authentifie l'utilisateur
        Auth::login($user);

        return redirect()->route('dashboard');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('landingPage');
    }
}
