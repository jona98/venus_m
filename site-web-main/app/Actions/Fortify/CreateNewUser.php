<?php

namespace App\Actions\Fortify;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Laravel\Jetstream\Jetstream;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array<string, string>  $input
     */
    public function create(array $input): User
    {
        $validator = Validator::make($input, [
            'pays_id' => 'required|integer|exists:pays,id',
            'pseudo' => 'required|string|unique:users',
            'telephone' => 'required|string|min:8|unique:users',
            'quartier' => 'required|string',
            'statut_professionnel' => 'required|string',
            'naissance' => 'required|date',
            'role_id' => 'required|integer',
            'genre_id' => 'required|integer',
            'password' => $this->passwordRules(),
            'terms' => Jetstream::hasTermsAndPrivacyPolicyFeature() ? ['accepted', 'required'] : '',
        ])->validate();

        $validator = Validator::make($input, $validator)
            ->after(function ($validator) use ($input) {
                $entete = substr($input['telephone'], 0, 2);
                if (intval($input['pays_id'] === 1)) {
                    if (!in_array($entete, User::arrayEntetesTG())) {
                        $validator->errors()->add('telephone', 'Veuillez fournir un numéro mobile Togolais valide');
                    }
                }
                // if (intval($request->country_id) === 2) {
                //     if (!in_array($entete, User::arrayEntetesGH())) {
                //         $validator->errors()->add('phone', 'Veuillez fournir un numéro mobile Ghanéen valide');
                //     }
                // }
                // if (intval($request->country_id) === 3) {
                //     if (!in_array($entete, User::arrayEntetesSN())) {
                //         $validator->errors()->add('phone', 'Veuillez fournir un numéro mobile Sénégalais valide');
                //     }
                // }
                // if (intval($request->country_id) === 4) {
                //     if (!in_array($entete, User::arrayEntetesCI())) {
                //         $validator->errors()->add('phone', 'Veuillez fournir un numéro mobile Ivorien valide');
                //     }
                // }
                // if (intval($request->country_id) === 5) {
                //     if (!in_array($entete, User::arrayEntetesBJ())) {
                //         $validator->errors()->add('phone', 'Veuillez fournir un numéro mobile Béninois valide');
                //     }
                // }
                // if (intval($request->country_id) === 6) {
                //     if (!in_array($entete, User::arrayEntetesBF())) {
                //         $validator->errors()->add('phone', 'Veuillez fournir un numéro mobile Burkinabais valide');
                //     }
                // }
                // if (intval($request->country_id) === 7) {
                //     if (!in_array($entete, User::arrayEntetesGB())) {
                //         $validator->errors()->add('phone', 'Veuillez fournir un numéro mobile Gabonais valide');
                //     }
                // }
                // if (intval($request->country_id) === 8) {
                //     if (!in_array($entete, User::arrayEntetesGB())) {
                //         $validator->errors()->add('phone', 'Veuillez fournir un numéro mobile Français valide');
                //     }
                // }
            });

        return User::create([
            'pays_id' => $input['pays_id'],
            'pseudo' => $input['pseudo'],
            'telephone' => $input['telephone'],
            'quartier' => $input['quartier'],
            'statut_professionnel' => $input['statut_professionnel'],
            'naissance' => $input['naissance'],
            'role_id' => $input['role_id'],
            'genre_id' => $input['genre_id'],
            'password' => Hash::make($input['password']),
        ]);
    }
}
