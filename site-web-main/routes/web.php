<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CinetPayController;
use App\Models\Client;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\CommandeProduitController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProduitController;
use App\Http\Controllers\UserController;
use App\Models\Genre;
use Illuminate\Support\Facades\Route;

//Auth
Route::get('/user/login', function () {
    return view('login');
})->name('user.login');

route::get('/user/register', function () {
    $genres = Genre::orderBy('titre', 'asc')->get();
    return view('register', compact('genres'));
})->name('user.register');

route::post('/user/auth/login', [AuthController::class, 'login'])->name('user.auth.login');
route::get('/user/auth/logout', [AuthController::class, 'logout'])->name('user.auth.logout');

Route::get('/', [HomeController::class, 'landingPage'])->name('landingPage');

Route::get('/favorite', function () {
    return view('favorite');
});



Route::get('/multi-step-form', function () {
    return view('multi-step-form');
});

Route::get('/company-register', function () {
    return view('company-register');
});



route::post('registerclient', [ClientController::class, 'NewClient']);


Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', [UserController::class, 'dashboard'])->name('dashboard');
});

//Produit
Route::get('/produits', [ProduitController::class, 'index'])->name("produit.index");
Route::get('/produits/category/{categoryProduit}', [ProduitController::class, 'produitByCategory'])->name("produit.byCategory");
Route::get('/produits/sub-category/{sousCategoryProduit}', [ProduitController::class, 'produitByCategory'])->name("produit.bySousCategory");
Route::get('/produit/detail/{produit}', [ProduitController::class, 'show'])->name("produit.detail");

//Cart
Route::get('/cart/checkout', [CartController::class, 'checkout'])->name("cart.checkout");
Route::post('/cart/add-item', [CartController::class, 'addItem'])->name("cart.addItem");

//Commande
Route::middleware([
    'auth:sanctum'
])->post('/commande/create', [CommandeProduitController::class, 'create'])->name("commande.create");

Route::any('/paiement-return', [CinetPayController::class, 'paiementReturn'])
    ->name('cinetpay.paiement_return');
