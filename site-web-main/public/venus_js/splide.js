document.addEventListener("DOMContentLoaded", function () {
  new Splide(".splide", {
    // type: "slide",
    // perPage: 6,
    // perMove: 1,
    // rewind: true,
  }).mount();
});

document.addEventListener("DOMContentLoaded", function () {
  new Splide("#carousel1").mount();

  // ============  second carousel "probleme:  Lorsque la page se recharge, le code JavaScript s'exécute après le chargement complet de la page, et à ce moment-là, la largeur de la fenêtre est déjà connue. C'est pourquoi j'ai changé d'approche en utilisant du css." =================
  //
  new Splide("#carousel2").mount();

  // var carousel2 = new Splide("#carousel2");

  // function updatePerPage2() {
  //   if (window.innerWidth <= 375) {
  //     carousel2.options.perPage = 1;
  //   } else if (window.innerWidth <= 480) {
  //     carousel2.options.perPage = 2;
  //   } else if (window.innerWidth <= 768) {
  //     carousel2.options.perPage = 3;
  //   } else if (window.innerWidth <= 1024) {
  //     carousel2.options.perPage = 4;
  //   } else {
  //     carousel2.options.perPage = 5;
  //   }
  //   carousel2.refresh();
  // }

  // updatePerPage2();

  // // Mettre à jour le nombre de balises li affichées lors du redimensionnement de la fenêtre
  // window.addEventListener("resize", updatePerPage2);

  // carousel2.mount();
});
